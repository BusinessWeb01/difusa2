jQuery.noConflict();
jQuery(document).ready(function($) {
	$(document).ready(function(){
		$("#invoice").click(function(){
			if(!$("#let_invoice").is(':checked')){
				$.prompt("Debe de aceptar los términos y condiciones");
			}
		});
		$("#order").click(function(){
			if(!$("#let_invoice").is(':checked')){
				$.prompt("Debe de aceptar los términos y condiciones");
			}
		});
	});
//$(window).load(function () {
	jQuery("a[rel^='prettyPhoto']").prettyPhoto();
	jQuery("#flexisel-carousel-trademarks").flexisel({
		visibleItems: 5,
		animationSpeed: 1200,
		autoPlay: true,
		autoPlaySpeed: 300,
		pauseOnHover: true,
		enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
			portrait: { 
                changePoint: 480,
                visibleItems: 1
            },
            tablet: { 
                changePoint: 769 ,//768
                visibleItems: 3
            }
        }
	});
	
	jQuery("#flexisel-carousel-other-products").flexisel({
		visibleItems: 3,
		animationSpeed: 1200,
		autoPlay: true,
		enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
			portrait: { 
                changePoint: 480,
                visibleItems: 1
            },
            tablet: { 
                changePoint: 769, //768
                visibleItems: 3
            }
        }
	});
	
	jQuery(".flexisel-carousel-other-categories").flexisel({
		visibleItems: 4,
		animationSpeed: 200,
		autoPlay: true,
		enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
			portrait: { 
                changePoint: 480,
                visibleItems: 1
            },
            tablet: { 
                changePoint: 769 ,//
                visibleItems: 3
            }
        }
	});
	
	jQuery('#slider-gallery').nivoSlider({
		manualAdvance: true, // Force manual transitions
        prevText: '&lsaquo;', // Prev directionNav text
        nextText: '&rsaquo;' // Next directionNav text
	});

	jQuery("#flexisel-carousel-gallery").flexisel({
		visibleItems: 3,
		animationSpeed: 200,
		enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
			portrait: { 
                changePoint: 480,
                visibleItems: 1
            },
            tablet: { 
                changePoint: 769 ,//768,
                visibleItems: 3
            }
        }
	});
	
	jQuery('.nbs-flexisel-nav-left').html('&lsaquo;');
	jQuery('.nbs-flexisel-nav-right').html('&rsaquo;');
	
	jQuery('.carousel-gallery .nivoLink').click(function(){
		var targetImage = $(this).attr('rel');
		console.log(targetImage);
		var slideNo = $('.slider-gallery').data('nivo:vars').currentSlide; 
		console.log(slideNo);
		if(targetImage != slideNo){
			jQuery('.slider-gallery-wrapper .nivo-control').eq(targetImage).trigger('click');
		}
		return false;
	});
	var optin_pay = $( "input:radio[name=payment_options]:checked" ).val();
	if (optin_pay == 'pagofacil') {
		jQuery('.credishow').show();
	}
	else{
		jQuery('.credishow').hide();
	}
	$("input[name=payment_options]:radio").change(function(){
		var select = $( "input:radio[name=payment_options]:checked" ).val();
		if (select == 'pagofacil') {
			jQuery('.credishow').show();
		}
		else{
			jQuery('.credishow').hide();
		}
	});
	
	jQuery('a#invoice').click(function(){
		if(jQuery("#let_invoice").is(":checked", true)){ 	
			jQuery('#client').show();
			jQuery('#payment').hide();
		}
		return false;
	});
	jQuery('a#order').click(function(){
		if(jQuery("#let_invoice").is(":checked", true)){
			jQuery('#payment').show();
			jQuery('#client').hide();
		}
		return false;
	});

	jQuery('#client_form').validate({
		errorPlacement: function(label, element){
			label.addClass('help-inline');
			label.insertAfter(element);
		},
		wrapper: 'span'
	});
	
	jQuery('#client_form').validate({
		errorPlacement: function(label, element){
			label.addClass('help-inline');
			label.insertAfter(element);
		},
		wrapper: 'span'
	});
	
	jQuery('#contact_form').validate({
		errorPlacement: function(label, element){
			label.addClass('help-inline');
			label.insertAfter(element);
		},
		wrapper: 'span'
	});
	jQuery('#payment_search').validate({
		errorPlacement: function(label, element){
			label.addClass('help-inline');
			label.insertAfter(element);
		},
		wrapper: 'span'
	});
	jQuery("input[name='check_shipping']").click(function(){
		if ($(this).val() === '1') {
			jQuery("#form_shipping").toggle();
		}
	});
	if (jQuery('#check_shipping').is(":checked"))
	{
	  jQuery("#form_shipping").hide();
	}
	
	jQuery('#paypal_back').click(function(){
		window.history.back();	
	});
	
	jQuery('#client_payment').validate({
		rules :{
			numbercredicard : {
				required : true,
				creditcard: true
			},
			year_expiration : {
				required : true,
				number : true,
				minlength : 2,
				maxlength : 2
			},
			cvv_code : {
				required : true,
				number : true,
				minlength : 3,
				maxlength : 4
			}
		},
    errorPlacement: function(label, element) {
        label.addClass('help-inline');
        label.insertAfter(element);
    },
    wrapper: 'span'
});
    jQuery.validator.addMethod("rfc", function(value, element) {
     return this.optional(element) || value.length == 13 || value.length == 12;
    }, jQuery.format("Por favor revise el RFC, tiene que tener 12 o 13 caractéres."));
           
    /*jQuery("#rfc").validate({
      rules: {
        somefield: {
          exactlength: 13
        }
       });*/
           
	$.mask.definitions['h'] = "[0-9a-zA-Z ]";
	$.mask.definitions['f'] = "[0-9()-]";
	$(".mask_name").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_last_name").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_phone").mask("?fffffffffffffffff",{placeholder:""});
	$(".mask_phone_office").mask("?fffffffffffffffff",{placeholder:""});
	$(".mask_address").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_city").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_neighborhood").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_postal_code").mask("?99999",{placeholder:""});
	$(".mask_rfc").mask("?hhhhhhhhhhhhh",{placeholder:""});
	$(".mask_trade_name").mask("?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});
	$(".mask_promotion_code").mask("?********************",{placeholder:""});
});