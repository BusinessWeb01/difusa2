<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registra_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('security');
			$this->load->model('registra_model');
			$this->load->model('panel_model');
	}
	function index()
	{
		$news = $this->panel_model->getNews();
		$states = $this->showStates();
		$send = array(
					'news' => $news,
					'estados' => $states
				);
		$this->load->view('registra_view',$send);
	}
	function registrar()
	{
		$states = $this->showStates();
		$nuevoCorreo = $this->input->post('usuario',TRUE);
		$correo = $this->registra_model->getMails();
		$bandera = true;
		for($c = 0 ; $c < count($correo) ; $c++)
		{			
			if(strcmp($correo[$c]['mails'],$nuevoCorreo) == 0)
			{
				$send = array(
					'error2' => 'correo',
					'news' => ($this->panel_model->getNews()),
					'estados' => $states
				);
				$bandera = false;
				$this->load->view('registra_view',$send);
				break;
			}			
		}
		if($bandera == true)
		{
			$this->registrarOk();
		}
	}
	function registrarOk()
	{
		$password = $this->input->post('password',TRUE);
		$password2 = $this->input->post('password2',TRUE);
		$mailArray = array();
		if(strcmp($password,$password2) == 0)
		{
			$password = do_hash($password,'md5');
			$data = array(
				"client_name" => $this->input->post('client_name',TRUE),
				"last_name_client" => $this->input->post('last_name_client',TRUE),
				"last_name2_client" => $this->input->post('last_name2_client',TRUE),
				"state" => $this->input->post('country',TRUE),
				"delegation" => $this->input->post('delegation',TRUE),
				"suburb" => $this->input->post('suburb',TRUE),
				"street" => $this->input->post('street',TRUE),
				"outdoor_number" => $this->input->post('outdoor',TRUE),
				"indoor_number" => $this->input->post('indoor',TRUE),
				"zipcode" => $this->input->post('zipcode',TRUE),
				"contact_mail" => $this->input->post('usuario',TRUE),
				"phone_number" => $this->input->post('local',TRUE),
				"cellphone_number" => $this->input->post('celular',TRUE),
				"user_name" => $this->input->post('usuario',TRUE),
				"password" => $password,
				'activated' => 0
			);
			$mailArray['datos_usuario'] = $data;
			$this->registra_model->registro($data);
			$data2 = array(
				"street_bussiness" => $this->input->post('street2',TRUE),
				"outdoor_number_bussiness" => $this->input->post('outdoor2',TRUE),
				"indoor_number_bussiness" => $this->input->post('indoor2',TRUE),
				"suburb_bussiness" => $this->input->post('suburb2',TRUE),
				"delegation_bussiness" => $this->input->post('delegation2',TRUE),
				"state_bussiness" => $this->input->post('country2',TRUE),
				"zipcode_bussiness" => $this->input->post('zipcode2',TRUE),
				"rfc" => $this->input->post('rfc',TRUE),
				"email_bussiness" => $this->input->post('email2',TRUE),
				"phone_number_bussiness" => $this->input->post('local2',TRUE),
				"cellphone_number_bussiness" => $this->input->post('celular2',TRUE)
				);
			$mailArray['datos_facturacion'] = $data2;
			$this->registra_model->registroFactura($data2);
			$this->sendingRegisterMail($mailArray);
			if($this->input->post('suscribir') != NULL)
			{
				$suscribe = $this->input->post('newsuser',TRUE);				
				$mandaSuscripcion = array();				
				for($c=0;$c<count($suscribe);$c++)
				{
					$mandaSuscripcion[$c] = array(
						'id_user' => $this->registra_model->newUserSuscription(),
						'id_subscription' => $suscribe[$c]
						);
				}
				$this->registra_model->insertNewSuscription($mandaSuscripcion);
			}
			$arreglotemp = array('registrook' => true);
			$this->session->set_userdata($arreglotemp);
			redirect(base_url());
		}
		else
		{
			$states = $this->showStates();
			$send = array(
					'error' => 'contraseñas',
					'estados' => $states,
					'news' => ($this->panel_model->getNews())
				);
			$this->load->view('registra_view',$send);
		}
	}

	public function sendingRegisterMail($userData)
	{
		$this->load->library('email');
		$configGmail = array(
 			'protocol' => 'smtp',
 			'smtp_host' => 'ssl://smtp.gmail.com',
 			'smtp_port' => 465,
 			'smtp_user' => 'difusastore@gmail.com',
 			'smtp_pass' => 'alesana12',
 			'mailtype' => 'html',
 			'charset' => 'utf-8',
 			'newline' => "\r\n"
 			);
		/*$estado = $this->registra_model->getStateMail($userData['datos_usuario']['state']);
		$municipio = $this->registra_model->getMunicipalMail($userData['datos_usuario']['delegation']);
		$localidad = $this->registra_model->getLocalityMail($userData['datos_usuario']['suburb']);
		$estado2 = $this->registra_model->getStateMail($userData['datos_facturacion']['state_bussiness']);
		$municipio2 = $this->registra_model->getMunicipalMail($userData['datos_facturacion']['delegation_bussiness']);
		$localidad2 = $this->registra_model->getLocalityMail($userData['datos_facturacion']['suburb_bussiness']);*/
		$usuarioID = $this->panel_model->getLastClient();
		$this->email->initialize($configGmail);
 		$this->email->from('difusastore@gmail.com',"DIFUSA");
 		$this->email->reply_to('contacto@difusa.com.mx','DIFUSA');
 		$this->email->to($userData['datos_usuario']['contact_mail']);
 		$this->email->subject('Registro DIFUSA');
 		$this->email->message("Buen dia ".$userData['datos_usuario']['client_name']." ".$userData['datos_usuario']['last_name_client']."<br><br>
			
			Los datos con los cuales usted indico ser registrado son los siguientes:<br><br>
			
			<h4>Datos Personales</h4>

			<b>Nombre:</b> ".$userData['datos_usuario']['client_name']."<br>
			<b>Apellidos:</b> ".$userData['datos_usuario']['last_name_client']." ".$userData['datos_usuario']['last_name2_client']."<br>
			"./*<b>Estado:</b> ".$estado[0]['nombre_estado']."<br>
			<b>Municipio:</b> ".$municipio[0]['nombre_municipio']."<br>
			<b>Colonia:</b> ".$localidad[0]['nombre_localidad']."<br>
			<b>Calle:</b> ".$userData['datos_usuario']['street']."<br>*/"
			<b>Estado:</b> ".$userData['datos_usuario']['state']."<br>
			<b>Municipio:</b> ".$userData['datos_usuario']['delegation']."<br>
			<b>Colonia:</b> ".$userData['datos_usuario']['suburb']."<br>
			<b>Número Exterior:</b> ".$userData['datos_usuario']['outdoor_number']." <b>Número Interior:</b> ".$userData['datos_usuario']['indoor_number']."<br>
			<b>C.P.:</b> ".$userData['datos_usuario']['zipcode']."<br>
			<b>Número Telefonico:</b> ".$userData['datos_usuario']['phone_number']."<br>
			<b>Número Celular:</b> ".$userData['datos_usuario']['cellphone_number']."<br>
			<b>Correo de Contacto:</b> ".$userData['datos_usuario']['contact_mail']."<br><br>
			
			<h4>Datos de Facturacion</h4>

			<b>RFC:</b> ".$userData['datos_facturacion']['rfc']."<br>
			"./*<b>Estado:</b> ".$estado2[0]['nombre_estado']."<br>*/"
			/*<b>Estado:</b> ".$userData['datos_facturacion']['state_bussiness']."<br>*/
			"./*<b>Municipio:</b> ".$municipio2[0]['nombre_municipio']."<br>*/"
			<b>Municipio:</b> ".$userData['datos_facturacion']['delegation_bussiness']."<br>
			"./*<b>Colonia:</b> ".$localidad2[0]['nombre_localidad']."<br>*/"
			<b>Colonia:</b> ".$userData['datos_facturacion']['suburb_bussiness']."<br>
			<b>Calle:</b> ".$userData['datos_facturacion']['street_bussiness']."<br>
			<b>Número Exterior:</b> ".$userData['datos_facturacion']['outdoor_number_bussiness']." <b>Número Interior:</b> ".$userData['datos_facturacion']['indoor_number_bussiness']."<br>
			<b>C.P.:</b> ".$userData['datos_facturacion']['zipcode_bussiness']."<br>
			<b>Número Telefonico:</b> ".$userData['datos_facturacion']['phone_number_bussiness']."<br>
			<b>Número Celular:</b> ".$userData['datos_facturacion']['cellphone_number_bussiness']."<br>
			<b>Correo de Contacto:</b> ".$userData['datos_facturacion']['email_bussiness']."<br><br>

			<h4>Datos de inicio de sesión</h4>
			<b>Usuario:</b> ".$userData['datos_usuario']['user_name']."<br><br>
			Por favor haga clic en este <a href='".base_url()."activar/".$usuarioID[0]['max']."'>enlace</a> para activar su cuenta
			<br><br>

			<center><h3>El equipo de DIFUSA le da la más cordial bienvenida a nuestro sistema de compras.</h3></center>
 			");
 		$this->email->send();
 		log_message('debug','resultado del correo'.$this->email->print_debugger());
	}
	function showStates()
	{
		$estados = $this->registra_model->getStates();
		$mandaEstados = "";
		foreach ($estados as $key) 
		{
			$mandaEstados .= "<option value='".$key['id']."'>".$key['nombre_estado']."</option>";
		}
		return $mandaEstados;
	}

	function showMun()
	{
		$idState = $this->input->post('id_state',TRUE);
		if($idState == 0)
		{
			echo "<option value='0'>Selecciona tu Municipio</option>";	
		}
		else
		{
			$municipios = $this->registra_model->getMunicipal($idState);
			echo "<option value='0'>Selecciona tu Municipio</option>";
			foreach($municipios as $key)
			{
				echo "<option value='".$key['id']."'>".$key['nombre_municipio']."</option>";
			}
		}
	}

	function showLocality()
	{
		$idMun = $this->input->post('id_mun',TRUE);
		if($idMun == 0)
		{
			echo "<option value='0'>Selecciona tu Localidad</option>";
		}
		else
		{
			$localidades = $this->registra_model->getLocality($idMun);
			echo "<option value='0'>Selecciona tu Localidad</option>";
			foreach ($localidades as $key) 
			{
				echo "<option value='".$key['id']."'>".$key['nombre_localidad']."</option>";	
			}
		}
	}
}