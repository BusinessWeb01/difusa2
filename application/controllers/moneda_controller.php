<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Moneda_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('lb_moneda');
		$this->load->library('carousel');
		$this->load->helper('url');
		$this->load->library('cart');
	}
	function changueCurrency()
	{	
		$monedaDestino1 = $this->input->post('moneda_destino',TRUE);
		$monedaOrigen = $this->input->post('moneda_origen',TRUE);
		log_message('debug', '$_POST["moneda_destino"] '.$monedaDestino1.' y $_POST["moneda_origen"]'.$monedaOrigen);
		switch($monedaDestino1)
		{
			case "USD":
				$monedaDestino = "Dolar Americano";
				break;
			case "MXN":
				$monedaDestino = "Peso Mexicano";
				break;
		}
		switch ($monedaOrigen)
		{
			case 'Peso Mexicano':
				$monedaEnvioOrigen = 'MXN';
				break;
			case 'Dolar Americano':
				$monedaEnvioOrigen = 'USD';
				break;
		}
		$this->session->set_userdata('origen',$monedaDestino);
		$this->session->set_userdata('origen_cambio',$monedaEnvioOrigen);
		$this->session->set_userdata('destino_cambio',$monedaDestino1);
		$send = array(
			'carousel_config' => $this->carousel->home()
		);
		redirect($_SERVER['HTTP_REFERER'],$send);
	}
}
?>