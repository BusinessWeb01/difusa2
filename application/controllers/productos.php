<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->library('carousel');
			$this->load->model('md_productos');
	}

	function levadura_liquida()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'LVL';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function levadura_seca()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'LVS';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function lupulo()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'LUP';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function malta()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'MAL';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function nutriente_de_levadura()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'NUL';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function coadyuvantes()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'COA';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function kegs()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'KEG';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function kits_sensoriales()
	{
		log_message('debug', 'md_productos->producto($prod)');

			$prod = 'KIT';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_productos->producto($prod)
			);
 
				$this->load->view('productos',$send);
		
		log_message('debug', 'md_productos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}

}

?>