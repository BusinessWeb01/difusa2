<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informacion_complementos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_informacion');
		$this->load->model('md_insumos');
	}

	function producto($id)
	{
		$infoGral = $this->md_informacion->prod($id);
		$cantidad = $infoGral[0]['price'];
		$divisaOrigen = $infoGral[0]['currency_name'];
		$divisaDestino = $this->session->userdata('destino_cambio');
		$sacos = $this->md_insumos->getPresentationBags();
		if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
		{
			for($d=0;$d<count($sacos);$d++)
			{
				if($infoGral[0]['id_presentation'] == $sacos[$d]['id_presentation'] && $infoGral[$c]['id_product'] != 2039)
				{
					switch($infoGral[0]['id_presentation'])
					{
						case 20:
							$cantidad *= 20;
							break;
						case 21:
							$cantidad *= 22.68;
							break;
						case 22:
							$cantidad *= 25;
						break;
					}
				}
			}
			if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
			{
				$valor = $cantidad / $this->session->userdata('dolarAPeso');
				$infoGral[0]['price'] = number_format($valor,2,'.','');
			}
			if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
			{
				$valor = $cantidad * $this->session->userdata('dolarAPeso');
				$infoGral[0]['price'] = number_format($valor,2,'.','');
			}
			$infoGral[0]['currency_name'] = $divisaDestino;
		}
		$user = $this->session->userdata('id_client');
		$verifica = $this->md_informacion->getWish($user,$id);
		if($verifica)
		{
			$send = array(
				'carousel_config' => $this->carousel->informacion(),
				'info'=>$infoGral,
				'wish' => 'ok'
			);
			$this->load->view('informacion_complementos',$send);
		}
		else
		{
			$send = array(
				'carousel_config' => $this->carousel->informacion(),
				'info'=>$infoGral
			);
			$this->load->view('informacion_complementos',$send);
		}
	}
	function comenta()
    {
    	$usuario = $this->input->post('clientId',TRUE);
    	$comentario = $this->input->post('comment',TRUE);
    	$producto = $this->input->post('idProducto',TRUE);
    	
    	
    }
}
?>