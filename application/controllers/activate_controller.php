<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activate_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->model('activar_model');
			$this->load->library('carousel');
	}

	function activar($idActivacion)
	{
		$activado = $this->activar_model->activar($idActivacion);
		if($activado)
		{
			$this->session->set_userdata('activado', true);
			redirect(base_url());
		}
		else
		{
			$this->session->set_userdata('activaerror',true);
			redirect(base_url());
		}
	}
}