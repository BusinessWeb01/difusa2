<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pago extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_complementos');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
   		$this->load->model('panel_model');
	}

	function exito()
	{
		echo "<script language='javascript'>alert('Compra Exitosa');</script>";
		$send = array(
			'carousel_config' => $this->carousel->home(),
			'section_nav' => 'home'
		);
		$s = $this->session->userdata('origen');
		if($s == NULL)
		{
			$monedas = array(
				'origen' => 'Peso Mexicano',
				'destino_cambio' => 'MXN'
				);
			$this->session->set_userdata($monedas);
		}
		load_view('home', $send);
	}

}