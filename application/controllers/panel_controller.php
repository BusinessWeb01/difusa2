<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Panel_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->model('panel_model');
			$this->load->library('lb_moneda');
			$this->load->model('registra_model');
	}

	function hola()
	{
		$this->load->view('panel_view');
		$this->load->view('bienvenida_view');
	}

	function orders()
	{
		$idUser = $this->session->userdata('id_client');
		$orders = $this->panel_model->get_orders($idUser);
		$send = array(
			'orders' => $orders
			);
		$this->load->view('panel_view');
		$this->load->view('orders_view',$send);
	}

	function info()
	{
		$idUser = $this->session->userdata('id_client');
		$clientData = $this->panel_model->getUserData($idUser);
		$clientDataBill = $this->panel_model->getUserBillData($idUser);
		log_message('debug','datos del cliente comunes '.print_r($clientData,TRUE));
		//log_message('debug','datos de facturacion '.print_r($clientDataBill,TRUE));
		/*$estados = $this->getState($clientData[0]['state']);
		$municipios = $this->getMunicipal($clientData[0]['state'],$clientData[0]['delegation']);
		$localidades = $this->getLocality($clientData[0]['delegation'],$clientData[0]['suburb']);
		$estadoBill = $this->getState($clientDataBill[0]['state_bussiness']);
		$municipioBill = $this->getMunicipal($clientDataBill[0]['state_bussiness'],$clientDataBill[0]['delegation_bussiness']);
		$localidadBill = $this->getLocality($clientDataBill[0]['delegation_bussiness'],$clientDataBill[0]['suburb_bussiness']);*/
		$send = array(
				"info"=> $clientData,
				"info2" => $clientDataBill
				/*'estados' => $estados,
				'municipio' => $municipios,
				'localidad' => $localidades,
				'estadoBill' => $estadoBill,
				'municipioBill' => $municipioBill,
				'localidadBill' => $localidadBill*/
			);
		$this->load->view('panel_view');
		$this->load->view('info_view',$send);
	}

	function comments()
	{
		$idUser = $this->session->userdata('id_client');
		$comments = $this->panel_model->getComments($idUser);
		$send = array('comment' => $comments);
		$this->load->view('panel_view');
		$this->load->view('comment_view',$send);	
	}

	function refund()
	{
		$idUser = $this->session->userdata('id_client');
		$refunds = $this->panel_model->getRefund($idUser);
		$send = array('refund' => $refunds);
		$this->load->view('panel_view');
		$this->load->view('refund_view',$send);		
	}

	function news()
	{
		$idUser = $this->session->userdata('id_client');
		$news = $this->panel_model->getNewsUser($idUser);
		$generalNews = $this->panel_model->getNews();
		$send = array('news_user' => $news,
						'news' => $generalNews);
		$this->load->view('panel_view');
		$this->load->view('news_view',$send);		
	}

	function wishes()
	{
		$idUser = $this->session->userdata('id_client');
		$wishes = $this->panel_model->getwishes($idUser);
		$send = array('wishes' => $wishes);
		$this->load->view('panel_view');
		$this->load->view('wishes_view',$send);		
	}

	function update()
	{
		$idUser = $this->session->userdata('id_client');
		$data = array(
			"client_name" => $this->input->post('nombre',TRUE),
			"last_name_client" => $this->input->post('a_paterno',TRUE),
			"last_name2_client" => $this->input->post('a_materno',TRUE),
			"state" => $this->input->post('country',TRUE),
			"delegation" => $this->input->post('delegation',TRUE),
			"suburb" => $this->input->post('suburb',TRUE),
			"street" => $this->input->post('street',TRUE),
			"outdoor_number" => $this->input->post('outdoor',TRUE),
			"indoor_number" => $this->input->post('indoor',TRUE),
			"zipcode" => $this->input->post('zipcode',TRUE),
			"contact_mail" => $this->input->post('email',TRUE),
			"phone_number" => $this->input->post('local',TRUE),
			"cellphone_number" => $this->input->post('celular',TRUE),
			"user_name" => $this->input->post('usuario',TRUE),
			"contact_mail" => $this->input->post('usuario',TRUE)
		);
		$data2 = array(
			"street_bussiness" => $this->input->post('street2',TRUE),
			"outdoor_number_bussiness" => $this->input->post('outdoor2',TRUE),
			"indoor_number_bussiness" => $this->input->post('indoor2',TRUE),
			"suburb_bussiness" => $this->input->post('suburb2',TRUE),
			"locality_bussiness" => $this->input->post('locality2',TRUE),
			"delegation_bussiness" => $this->input->post('delegation2',TRUE),
			"state_bussiness" => $this->input->post('country2',TRUE),
			"zipcode_bussiness" => $this->input->post('zipcode2',TRUE),
			"rfc" => $this->input->post('rfc',TRUE),
			"email_bussiness" => $this->input->post('email_bussiness',TRUE),
			"phone_number_bussiness" => $this->input->post('phone_number_bussiness',TRUE)
		);
		$this->panel_model->updateUser($data,$data2,$idUser);
		$this->session->set_userdata('actualiza',true);
		$clientData = $this->panel_model->getUserData($idUser);
		$send = array("info"=> $clientData);		
		/*$this->load->view('panel_view');
		$this->load->view('info_view',$send);*/
		redirect($_SERVER['HTTP_REFERER']);
	}

	function details($orden)
	{
		$idUser = $this->session->userdata('id_client');
		$products = $this->panel_model->getDetails($orden,$idUser);
		$send = array('detail' => $products);
		$this->load->view('panel_view');
		$this->load->view('orderdetails_view',$send);
	}

	function updateNews()
	{
		$idUser = $this->session->userdata('id_client');
		$suscripciones = $this->input->post('suscripciones',TRUE);
		$generalNews = $this->panel_model->getNews();
		$insertar = array();
		for($c = 0 ; $c<count($generalNews) ; $c++)
			{
				if(isset($suscripciones[$c]))
				{
					$insertar[$c] = array('id_user' => $idUser,
								'id_subscription' => $suscripciones[$c]
						);
				}
			}
		if(count($insertar) < 1 )
		{
			$this->panel_model->noSuscription($idUser);
		}
		else
		{
			$this->panel_model->updateSuscription($idUser,$insertar);
		}
		$this->session->set_userdata('suscripcion',true);
		$this->news();
	}

	function getState($stateUser)
	{
		$estados = $this->registra_model->getStates();
		$mandaEstados = "";
		foreach ($estados as $key)
		{
			if($key['id'] == $stateUser)
				$mandaEstados .= "<option value='".$key['id']."' selected>".$key['nombre_estado']."</option>";
			else
				$mandaEstados .= "<option value='".$key['id']."'>".$key['nombre_estado']."</option>";
		}
		return $mandaEstados;
	}

	function getMunicipal($stateUser,$municipalUser)
	{
		$municipios = $this->registra_model->getMunicipal($stateUser);
		$mandaMunicipios = "";
		foreach ($municipios as $key)
		{
			if($key['id'] == $municipalUser)
				$mandaMunicipios .= "<option value='".$key['id']."' selected>".$key['nombre_municipio']."</option>";
			else
				$mandaMunicipios .= "<option value='".$key['id']."'>".$key['nombre_municipio']."</option>";
		}
		return $mandaMunicipios;
	}

	function getLocality($municipalUser,$localityUser)
	{
		$localidades = $this->registra_model->getLocality($municipalUser);
		$mandaLocalidades = "";
		foreach ($localidades as $key)
		{
			if($key['id'] == $localityUser)
				$mandaLocalidades .= "<option value='".$key['id']."' selected>".$key['nombre_localidad']."</option>";
			else
				$mandaLocalidades .= "<option value='".$key['id']."'>".$key['nombre_localidad']."</option>";
		}
		return $mandaLocalidades;
	}
}