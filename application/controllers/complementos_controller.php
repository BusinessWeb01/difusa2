<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Complementos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('md_complementos');
		$this->load->model('md_insumos');
		$this->load->library(array('pagination', 'cart'));
	}

	function productos2($subcategoria,$pagina)
	{
		$infoGral = '';
		if($pagina == 1)
		{
			$inicio = 0;
			$fin = 8;
			$this->session->set_userdata('fin',8);
		}
		else if($pagina >1)
		{
			$inicio = ($pagina-1) * $this->session->userdata('fin');
			$fin = $this->session->userdata('fin');
		}
		$infoGral = $this->md_insumos->getProducts($subcategoria,$inicio,$fin);
		$cantidad2 = $this->md_insumos->getTotalProducts($subcategoria);
		$subcategoria2 = $this->md_insumos->getSubcategoryName($subcategoria);
		$sacos = $this->md_complementos->getPresentationBags();
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				for($d=0;$d<count($sacos);$d++)
				{
					log_message('debug','Entro al ciclo de sacos :v');
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
						$infoGral[$c]['price'] = $cantidad;
					}
				}
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad / $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
			else if((strcmp($divisaDestino, $divisaOrigen)) === 0)
			{
				log_message('debug','Entro a la condicional === 0');
				for($d=0;$d<count($sacos);$d++)
				{
					log_message('debug','Entro al ciclo de sacos :v');
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
						$infoGral[$c]['price'] = $cantidad;
					}
				}
			}
			if($infoGral[$c]['iva_tax'] == 1)
			{
				$cantidadSinIVA = $infoGral[$c]['price'];
				$cantidadSinIVA *= 1.16;
				$infoGral[$c]['price'] = $cantidadSinIVA;
			}
			log_message('debug','precio obtenido en el controlador complementos '.$infoGral[$c]['price']);
		}
		$send = array(
			'marca' => $this->md_insumos->marca($subcategoria),
			'presentacion' => $this->md_insumos->presentacion($subcategoria),
			'info'=> $infoGral,
			'pais' => $this->md_insumos->pais($subcategoria),
			'nombre' => $subcategoria2,
			'subcategoria' => $subcategoria,
			'total' => $cantidad2,
			'pagina' => $pagina
		);
		$this->load->view('complementos_view',$send);
	}

	/*function productos()
	{
		log_message('debug', 'md_complementos->producto($prod)');
			$prod = 'COA';
			$inicio= 0;
			$fin= 8;
			$infoGral = $this->md_complementos->prod($prod,$inicio,$fin); //meto los productos en infoGral, para ir realizando la conversion de los precios y los vuelvo a guardar en los indices correspondientes
			for($c=0;$c<count($infoGral);$c++)
			{
				$cantidad = $infoGral[$c]['price'];
				$divisaOrigen = $infoGral[$c]['currency_name'];
				$divisaDestino = $this->session->userdata('destino_cambio');
				if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
				{
					if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoAEuro');
					if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoADolar');
					if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroAPeso');
					if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroADolar');
					if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
					if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
						$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAEuro');
					$infoGral[$c]['currency_name'] = $divisaDestino;
				}
			}
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'info2'=>$infoGral,
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>8,
				'numero'=>8,
				'ini'=>0,
				'pagina'=>1,
				'marca1'=>0,
				'presen1'=>0,
				'cat1'=>0,
				'pais1'=>0
			);
				$this->load->view('complementos',$send);
		
		log_message('debug', 'md_complementos->productos(INS).$this->db = '.print_r($this->db,TRUE));
	}*/

	
}
