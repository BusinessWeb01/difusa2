<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->model('login_model');
			$this->load->helper('security');
			$this->load->library('carousel');
	}
	function index()
	{	
		$this->load->view('login_view');
		if($this->session->userdata('id_client'))
		{
			redirect(base_url());
		}
	}
	function loguea()
	{
		$username = $this->input->post('username',TRUE);
		$password = $this->input->post('password',TRUE);
		$admin = $this->login_model->admin($username,$password);
		if($admin)
		{
			$this->session->set_userdata($admin[0]);
			$this->session->set_userdata('admin',true);
			redirect(base_url());
		}
		$password = do_hash($password,'md5');
		$send2 = $this->login_model->logueo($username,$password);
		if($send2 && !$admin)
		{
			if($send2[0]['activated'] == 0)
			{
				$send = array(
					'desactivado' => true
				);
				$this->load->view('login_view',$send);
			}
			else
			{
				$this->session->set_userdata($send2[0]);
				$arreglo = array("item" => true);
				$this->session->set_userdata($arreglo);
				redirect(base_url());
			}
		}
		else
		{
			$send = array(
				'error' => 'error'
			);
			$this->load->view('login_view', $send);
		}
	}
	function desloguea()
	{
		$this->session->unset_userdata('session_id');
		redirect(base_url());
	}
	function regresa()
	{		
		redirect(base_url());
	}
}
?>