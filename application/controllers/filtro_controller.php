<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Filtro_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('filtro_model','md_insumos'));
	}

	function filtrado($subcategoria,$pagina)
	{
		$post = $this->input->post();
		if($pagina == 1)
		{
			$inicio = 0;
			$fin = 8;
			$this->session->set_userdata('fin',8);
		}
		else if($pagina >1)
		{
			$inicio = ($pagina-1) * $this->session->userdata('fin');
			$fin = $this->session->userdata('fin');
		}
		log_message('debug',"datos del post del filtro ".print_r($post,TRUE));
		$nombre = "%";
		$marca = "";
		$pais = "";
		$presentacion = "";
		if((strlen($post['nombreProducto']) < 3) && (strcmp($post['marca'],'0') === 0) && (strcmp($post['pais'],'0') === 0) && (strcmp($post['presentacion'],'0') === 0))
		{
			$this->session->set_userdata('filtroempty',true);
			redirect($_SERVER['HTTP_REFERER']);
		}
		else
		{
			if(strlen($post['nombreProducto']) >= 3)
				$nombre .= $post['nombreProducto'].'%';
			else
				$nombre .= '%';

			if(strcmp($post['marca'],'0') !== 0)
				$marca = $post['marca'];
			else
				$marca = '%';

			if(strcmp($post['pais'],'0') !== 0)
				$pais = $post['pais'];
			else
				$pais = '%';

			if(strcmp($post['presentacion'],'0') !== 0)
				$presentacion = $post['presentacion'];
			else
				$presentacion = '%';
			$infoGral = $this->filtro_model->resultadosFiltro($nombre,$marca,$pais,$presentacion,$subcategoria,$inicio,$fin);
			$conteo = $this->filtro_model->resultadosFiltroTotal($nombre,$marca,$pais,$presentacion,$subcategoria);
			$sacos = $this->md_insumos->getPresentationBags();
		}
		for($c=0;$c<count($infoGral);$c++)
		{
			log_message('debug','Entro al ciclo de infoGral');
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				for($d=0;$d<count($sacos);$d++)
				{
					log_message('debug','valor del id de la presentacion en el filtro '.$infoGral[$c]['id_presentation']);
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
					}
				}
				log_message('debug','Entro a la condicional !== 0');
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad / $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				log_message('debug','precio guardado '.$infoGral[$c]['price']);
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
			else if((strcmp($divisaDestino, $divisaOrigen)) === 0)
			{
				for($d=0;$d<count($sacos);$d++)
				{
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
						$infoGral[$c]['price'] = $cantidad;
					}
				}
			}
			if($infoGral[$c]['iva_tax'] == 1)
			{
				$cantidadSinIVA = $infoGral[$c]['price'];
				$cantidadSinIVA *= 1.16;
				$infoGral[$c]['price'] = $cantidadSinIVA;
			}
		}
		$send = array(
			'marca' => $this->md_insumos->marca($subcategoria),
			'presentacion' => $this->md_insumos->presentacion($subcategoria),
			'pais' => $this->md_insumos->pais($subcategoria),
			'resultados' => $infoGral,
			'totalResultados' => $conteo,
			'subcategoria' => $subcategoria,
			'pagina' => $pagina,
			'seleccionPresentacion' => $presentacion,
			'seleccionPais' => $pais,
			'seleccionMarca' => $marca,
			'nombre2' => $nombre,
			);
		$this->load->view('filtro_results_view',$send);
	}
}