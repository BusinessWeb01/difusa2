<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nuevos_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('new_model');
		$this->load->library('Lb_carro');
   		$this->load->library('lb_moneda');
	}

	public function showNewProducts()
	{
		$infoGral = $this->new_model->getNewProducts();
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad / $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				$infoGral[$c]['price'] = number_format($infoGral[$c]['price'],2,'.','');
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
		}
		$send = array(
				'codigos' => $infoGral,
				'new' => 'ok'
			);
		$this->load->view('top_sales',$send);
	}
}