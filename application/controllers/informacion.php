<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informacion extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_informacion');
		$this->load->model('md_insumos');
		$this->load->library('cart');
	}

	function producto($id)
	{
		$infoGral = $this->md_informacion->prod($id);
		$cantidad = $infoGral[0]['price'];
		log_message('debug','precio obtenido en el controlador informacion1 '.$cantidad);
		$divisaOrigen = $infoGral[0]['currency_name'];
		log_message('debug','divisa obtenida al entrar a la funcion en informacion '.$divisaOrigen);
		$divisaDestino = $this->session->userdata('destino_cambio');
		log_message('debug','divisa obtenida al entrar a la funcion en informacion '.$divisaDestino);
		$sacos = $this->md_insumos->getPresentationBags();
		if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
		{
			log_message('debug','Las divisas son diferentes');	
			for($d=0;$d<count($sacos);$d++)
			{
				if($infoGral[0]['id_presentation'] == $sacos[$d]['id_presentation'] && $infoGral[0]['id_product'] != 2039)
				{
					switch($infoGral[0]['id_presentation'])
					{
						case 20:
							$cantidad *= 20;
							break;
						case 21:
							$cantidad *= 22.68;
							break;
						case 22:
							$cantidad *= 25;
						break;
					}
				}
			}
			if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad / $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
			$infoGral[0]['currency_name'] = $divisaDestino;
		}
		else if((strcmp($divisaDestino, $divisaOrigen)) === 0)
		{
			log_message('debug','Entro a la condicional === 0');
			for($d=0;$d<count($sacos);$d++)
			{
				if(($infoGral[0]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[0]['id_product'] != 2039)
				{
					log_message('debug','Entro al ciclo de sacos :v');
					switch($infoGral[0]['id_presentation'])
					{
						case 20:
							$cantidad *= 20;
							break;
						case 21:
							$cantidad *= 22.68;
						break;
						case 22:
							$cantidad *= 25;
						break;
					}
					$infoGral[0]['price'] = $cantidad;
				}
			}
		}
		log_message('debug','precio despues de la conversion '.$infoGral[0]['price']);
		if($infoGral[0]['iva_tax'] == 1)
		{
			$cantidadSinIVA = $infoGral[0]['price'];
			$cantidadSinIVA *= 1.16;
			$infoGral[0]['price'] = $cantidadSinIVA;
		}
		log_message('debug','precio obtenido en el controlador informacion '.$infoGral[0]['price']);
		$send = array(
			'carousel_config' => $this->carousel->informacion(),
			'info'=>$infoGral
		);
		$this->load->view('informacion',$send);
	}

}

?>