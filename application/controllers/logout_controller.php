<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logout_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('md_category');
	}
	function index()
	{	
		$this->load->view('logout_view');
	}
}