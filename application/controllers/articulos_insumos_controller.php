<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articulos_insumos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('md_insumos');
   		$this->load->library(array('pagination', 'cart','Lb_carro','carousel'));
	}

	function productos2($subcategoria,$pagina)
	{
		$infoGral = '';
		if($pagina == 1)
		{
			$inicio = 0;
			$fin = 8;
			$this->session->set_userdata('fin',8);
		}
		else if($pagina >1)
		{
			$inicio = ($pagina-1) * $this->session->userdata('fin');
			$fin = $this->session->userdata('fin');
		}
		$infoGral = $this->md_insumos->getProducts($subcategoria,$inicio,$fin);
		$cantidad2 = $this->md_insumos->getTotalProducts($subcategoria);
		$subcategoria2 = $this->md_insumos->getSubcategoryName($subcategoria);
		$sacos = $this->md_insumos->getPresentationBags();
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				for($d=0;$d<count($sacos);$d++)
				{
					log_message('debug','Entro al ciclo de sacos :v');
					log_message('debug','valor del id de la presentacion '.$infoGral[$c]['id_presentation']);
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
					}
				}
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad / $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
			else if((strcmp($divisaDestino, $divisaOrigen)) === 0)
			{
				log_message('debug','Entro a la condicional === 0');
				for($d=0;$d<count($sacos);$d++)
				{
					log_message('debug','Entro al ciclo de sacos :v');
					if(($infoGral[$c]['id_presentation'] == $sacos[$d]['id_presentation']) && $infoGral[$c]['id_product'] != 2039)
					{
						switch($infoGral[$c]['id_presentation'])
						{
							case 20:
								$cantidad *= 20;
								break;
							case 21:
								$cantidad *= 22.68;
							break;
							case 22:
								$cantidad *= 25;
							break;
						}
						$infoGral[$c]['price'] = $cantidad;
					}
				}
			}
		}
		$send = array(
				'marca' => $this->md_insumos->marca($subcategoria),
				'info'=> $infoGral,
				'nombre' => $subcategoria2,
				'presentacion' => $this->md_insumos->presentacion($subcategoria),
				'pais' => $this->md_insumos->pais($subcategoria),
				'subcategoria' => $subcategoria,
				'sacos' => $this->md_insumos->getPresentationBags(),
				'total' => $cantidad2,
				'pagina' => $pagina
			);
		$this->load->view('insumos_user_view',$send);
	}

	function agregarProducto()
    {
        $id = $_POST['idproduct'];
        $cantidad = $_POST['cantidad'];
        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $codigo = $_POST['codigo'];
        $moneda_origen = $_POST['moneda'];
		$moneda_destino=$this->session->userdata('destino_cambio');
		log_message('debug','moneda destino llega con el valor '.$moneda_destino);
		$this->lb_carro->agregar($id,$cantidad,$precio,$nombre,$codigo,$moneda_destino);
        $this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
     
		$prod = 'MAL';
		$inicio= 0;
		$fin= 8;
		$infoGral = $this->md_insumos->prod($prod,$inicio,$fin); //meto los productos en infoGral, para ir realizando la conversion de los precios y los vuelvo a guardar en los indices correspondientes
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoAEuro');
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoADolar');
				if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroAPeso');
				if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroADolar');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAEuro');

				$infoGral[$c]['price'] = number_format($infoGral[$c]['price'],2,'.','');
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
		}
		$this->session->set_userdata('agregado',true);
		redirect($_SERVER['HTTP_REFERER']."#visualizar");
    }    
    function insertWish()
    {
    	$producto = $this->input->post('idproduct',TRUE);
    	$user = $this->session->userdata('id_client');
    	$wishInsert = array(
    		'id_user' => $user,
    		'id_product' => $this->input->post('idproduct',TRUE)
    	);
    	$this->md_insumos->insertWish($wishInsert);
    	//echo "<script language='javascript'>alert('Producto agregado correctamente');</script>";
    	$this->session->set_userdata('deseo',true);
 		redirect($_SERVER['HTTP_REFERER'].'#visualizar',$send);
    }
}

?>