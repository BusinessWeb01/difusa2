<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Busqueda_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('search_model');
	}

	public function index($pagina)
	{
		if($this->input->post('parametro',TRUE) == NULL && $this->input->post('opcion',TRUE) == NULL)
		{
			$parametro = $this->session->userdata('parametro');
			$tipoDeBusqueda = $this->session->userdata('opcion');
			$paginaActual = $pagina;
			if($pagina == 1)
			{
				$inicio = 0;
			}
			else if($pagina > 1)
			{
				$inicio = ($pagina-1) * $this->session->userdata('fin');
			}
			$fin = $this->session->userdata('fin');
		}
		else
		{	
			$parametro = $this->input->post('parametro',TRUE);
			if(ord($parametro) === 0)
				$parametro = NULL;	
			else 
			{
				$parametro = "%".$parametro."%";
			}
			$tipoDeBusqueda = $this->input->post('opcion',TRUE);
			$inicio = 0;
			$fin = 9;
			$toSession = array(
				'parametro' => $parametro,
				'opcion' => $tipoDeBusqueda,
				'inicio' => $inicio,
				'fin' => $fin
			);
			log_message('debug','Datos recibidos'.print_r($toSession,TRUE));
			$this->session->set_userdata($toSession);
		}
		$infoGral = '';
		switch($tipoDeBusqueda)
		{
			case 'pais':
				$infoGral = $this->search_model->getProductsPerCountry($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerCountryTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'marca':
				$infoGral = $this->search_model->getProductsPerBrand($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerBrandTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'tipo':
				$infoGral = $this->search_model->getProductsPerType($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerTypeTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'codigo':
				$infoGral = $this->search_model->getProductsPerCode($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerCodeTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'subcategoria':
				$infoGral = $this->search_model->getProductsPerSub($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerSubTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'nombre':
				$infoGral = $this->search_model->getProductsPerName($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerNameTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
			case 'presentacion':
				$infoGral = $this->search_model->getProductsPerPresentation($parametro,$inicio,$fin);
				$total = $this->search_model->getProductsPerPresentationTotal($parametro);
				log_message('debug','Datos de la busqueda'.print_r($infoGral,TRUE));
				break;
		}

		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoAEuro');
				if(strcmp($divisaOrigen,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('pesoADolar');
				if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroAPeso');
				if(strcmp($divisaOrigen,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('euroADolar');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAPeso');
				if(strcmp($divisaOrigen,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
					$infoGral[$c]['price'] = $cantidad * $this->session->userdata('dolarAEuro');
				$infoGral[$c]['price'] = number_format($infoGral[$c]['price'],2,'.','');
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
		}
		$send = array(
				'info' => $infoGral,
				'parametro' => $parametro,
				'tipo' => $tipoDeBusqueda,
				'total' => $total,
				'pagina' => $pagina
			);
		if($this->session->userdata('id_client'))
			$this->load->view('result_user_view',$send);
		else
			$this->load->view('result_view',$send);
	}
}