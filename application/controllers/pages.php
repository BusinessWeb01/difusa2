<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->library('lb_moneda');
		$this->load->model('terms_model');
		$this->load->model('md_pages');
	}

	public function index()
	{
		if(isset($_SERVER['HTTP_REFERER']))
		{
			$direccion = $_SERVER['HTTP_REFERER'];
			$direccion = explode('/',$direccion);
			if(strcmp(end($direccion),'logout') === 0)
				$this->session->set_userdata('logout',true);
		}
		$pesoADolar = $this->lb_moneda->conversor_monedas();
		log_message('debug','valor obtenido del DOF '.$pesoADolar);
		$send = array(
			'carousel_config' => $this->carousel->home(),
			'section_nav' => 'home'
		);
		$s = $this->session->userdata('origen');
		if($s == NULL)
		{
			$monedas = array(
				
				'dolarAPeso' => $pesoADolar,
				'origen' => 'Peso Mexicano',
				'destino_cambio' => 'MXN'
				);
			$this->session->set_userdata($monedas);
		}
		$this->session->unset_userdata('fin');
		$this->session->unset_userdata('parametro');
		$this->session->unset_userdata('opcion');
		load_view('home', $send);
	}
	
	public function error_404(){
		load_view('error_404');
	}

	public function thanks(){
		$send = array(
			'carousel_config' => $this->carousel->thanks()
		);
		load_view('thanks', $send);
	}
	
	public function select($url = ''){
		$this->load->library('lb_pages');
		$send = $this->lb_pages->get_page($url);
		if($send){
			load_view('page', $send);
		}
		else{
			load_view('error_404');
		}
	}
	
	public function ingredients(){
		$send = array(
			'carousel_config' => $this->carousel->ingredients()
		);
		load_view('ingredients',$send);
	}
	
	public function products($product_type='',$category='',$sub_category='',$page=1){
		$this->load->library('lb_products');
		$send = $this->lb_products->get_content($product_type,$category,$sub_category,$page);
		load_view($send['page'],$send);
	}
	
	public function products_category($product_type='',$category='',$page=1){
		$this->products($product_type,$category,'',$page);
	}
	
	public function products_sub_category($product_type='',$category='',$sub_category='',$page=1){
		$this->products($product_type,$category,$sub_category,$page);
	}
	
	public function products_type($product_type='',$page=1){
		$this->products($product_type,'','',$page);
	}
	
	public function detail($product_code='',$slug=''){
		$this->load->library('lb_products');
		
		$send = array(
			'page' => 'detail',
			'product_detail' => $this->lb_products->get_product_detail($product_code,$slug),
			'first_title' => 'Detalle de producto',
		);
		load_view($send['page'],$send);
	}
	
	public function summary_cart(){
		$this->load->library('lb_summary_cart');
		$send = $this->lb_summary_cart->get_content();
		load_view('summary_cart', $send);
	}
	
		function thanks_payment(){
		$this->load->model('md_invoicing');
		$exists = false;
		$post = $this->input->post();
		$get = $this->input->get();

               //ndp 20150730 - boton de pruebas
               $CI = &get_instance();
               $CI->load->library('session');
               $session_user = $CI->session->all_userdata();
                //$temp = $CI->session->__get('folio');
               //log_message('debug','$session_user[folio]; = '.print_r($session_user['folio'],TRUE));

               /**
                * HARD CODE PARA PRUEBAS
                */
                //ndp 20150716 - hardcode para obtener una respuesta de vuelta
		
                $post = array(
                        'codigo' => 0,
                        'mensaje' => 'Pago exitoso',
                        'autorizacion' => 856866,
                        'referencia' => 55,
                        'referencia' => $session_user['folio'],
                        'importe' => '0.01',
                        'mediopago' => 'TDC',
                        'financiado' => 'REV',
                        'plazos' => 0,
                        's_transm' => 200040514,
                        'hash' => '20c7cfb4718251643faef988ed3b7e336a1e8b16a77e5a262ecf6db1f8d105b4',
                        //'tarjetahabiente' => 'DAVID ALDRIN GALVAN SANDOVAL'
                        'tarjetahabiente' => 'USUARIO DE PRUEBA'
		);
                
                /*
                $get = array(
                        't' => 20150811085415,
                        'f' => 102//$session_user['folio']
                );
		*/
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$GET='.print_r($get, TRUE));		
		log_message('debug', '$POST='.print_r($post, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
		
		$session_user = $this->session->all_userdata();
		$data = array(
			'post' => $post,
			'get' => $get,
			'session_user' => $session_user
		);
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$data array='.print_r($data, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
		
		$data = json_encode($data);

		
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$data json='.print_r($data, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');



                 $desplegarPantallaExito = 'no';
                 $carrito = null;//arreglo para el carrito en la pagina de gracias

                //ndp 20150811 - solo funciona para paypal, para la url de retorno
		if($get){
			//ndp 20150715 - Haciendo pruebas
			log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
			log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
			log_message('debug', 'ES GET');		
			log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');

			if(isset($get['t'])){
				if(isset($get['f'])){
					$this->load->library('lb_functions');
					$time = $this->lb_functions->get_the_time($get['t']);
					$this->load->model('md_pages');
					//$payment = $this->md_pages->get_info_of_payment($time, $get['f']);
                                        $payment = $this->md_pages->get_info_of_payment_by_folio($get['f']);


					//ndp 20150715 - Haciendo pruebas
					log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION get[t] get[f]: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');
					log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION get[t] get[f]: function thanks_payment() XXXXXXXXXXXXXXXXXX');
					log_message('debug', '$payment='.print_r($payment, TRUE));		
					log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
					
					if($payment){
						$this->md_invoicing->update_payment($data, $payment->id_pago);
						$this->lb_functions->create_file_for_sap($payment);
						$exists = true;
                                                $desplegarPantallaExito = 'si';

                                                $info_pago = $this->md_invoicing->get_informacion_pago($get['f']);
                                                $carrito = json_decode($info_pago['informacion'],TRUE);


					}



				}
			}else{

                            //error en el pago
                        }
		}
                ////////////////////////////////////////////////SE AGREGA ELSE IF
                else if($post){

		log_message('debug', '...post');
			if(isset($post['referencia'])){
			log_message('debug', '...referencia');
				$codigo = 999;
				if(isset($post['codigo'])){
					$codigo = $post['codigo'];
				}
				if(($codigo == 0) or ($codigo == 3)){
					log_message('debug', '...codigo de exito');
					
					$this->load->model('md_pages');

                                        //ndp 20150728 - esta es la modificacion

					$payment = $this->md_pages->get_info_of_payment_by_folio($post['referencia']);
					log_message('debug', '$payment='.print_r($payment, TRUE));		
					if($payment){
						$this->load->library('lb_functions');
						$this->md_invoicing->update_payment($data, $payment->id_pago);
						$this->lb_functions->create_file_for_sap($payment);
						$exists = true;

                                                $desplegarPantallaExito = 'si';

                                                $info_pago = $this->md_invoicing->get_informacion_pago($payment->id_pago);
                                                $carrito = json_decode($info_pago['informacion'],TRUE);
					}
				}
				else{
				}
			}
		}
		if(!$exists){
			$this->md_invoicing->save_no_payment($data);
		}
		$send = array(
			'carousel_config' => $this->carousel->thanks(),
                        'post' => $post,
                        'get' => $get,
                        'desplegarPantallaExito' => $desplegarPantallaExito,
                        'carrito'=> $carrito
		);
		load_view('thanks', $send);
	}

	function terms($slug)
	{
		$pagina['mostrar'] = $this->terms_model->get_news($slug);
		$this->load->view('terms_view',$pagina);
	}

	function load_remember_password()
	{
		$this->load->view('retrieve_password_view');
	}

	function remember_password()
	{
		$correo = $this->input->post('mail',TRUE);
		$row = $this->md_pages->verifyMail($correo);
		log_message('debug','datos traidos con el correo'.print_r($row,TRUE));
		if($row['user_name'])
		{
			$codigo = $this->sendMessage($row['contact_mail'],$row['id_client']);
			$send = array(
					'correo' => $correo,
					'codigo' => $codigo
				);
			log_message('debug','entro a la primera condicion del if');
			$this->load->view('recovery_form_view',$send);
		}
		if($row['activated'] == 0)
		{
			$send = array('desactivado' => true);
			redirect($_SERVER['HTTP_REFERER'],$send);
		}
		else
		{
			$send = array('error' => true);
			redirect($_SERVER['HTTP_REFERER'],$send);
		}
	}

	function sendMessage($correo,$cliente)
	{
		$this->load->library('email');
		$configGmail = array(
 			'protocol' => 'smtp',
 			'smtp_host' => 'ssl://smtp.gmail.com',
 			'smtp_port' => 465,
 			'smtp_user' => 'difusastore@gmail.com',
 			'smtp_pass' => 'alesana12',
 			'mailtype' => 'html',
 			'charset' => 'utf-8',
 			'newline' => "\r\n"
 			);
		$codigo = rand();
		$this->email->initialize($configGmail);
 		$this->email->from('difusastore@gmail.com',"DIFUSA");
 		$this->email->reply_to('contacto@difusa.com.mx','DIFUSA');
 		$this->email->to($correo);
 		$this->email->subject('Recuperación de contraseña');
 		$this->email->message("Buen dia<br><br>El codigo que debe usar para reasignar tu contraseña es ".$codigo."");
 		$this->email->send();
 		log_message('debug','resultado del correo'.$this->email->print_debugger());
 		return $codigo;
	}
	function resetPassword()
	{
		if($this->input->post('pass1',TRUE))
		{
			$contraseña = $this->input->post('pass1',TRUE);
			$contraseña = do_hash($contraseña,'md5');
			$bandera = $this->md_pages->cambiaPass($correo,$contraseña);
			if($bandera)
			{
				$this->session->set_userdata('Recuperado',true);
				redirect(base_url());
			}
			else
			{
				$this->session->set_userdata('Recuperadoerror',true);
				redirect(base_url());
			}
		}
		else
		{
			$this->load->view('recovery_form_view');
		}
	}
}