<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacto_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('contact_model');
			$this->load->library('carousel');
	}
	function send($option)
	{
		if(strcmp($option,'Contacto')==0)
		{
			load_view('contact');
		}
		elseif(strcmp($option,'Equipo')==0)
		{
			load_view('team_contact');
		}
	}
	function getDataComment()
	{
		sleep(4);
		log_message('debug','Entro a getDataComment');
		$nombre = $this->input->post('name',TRUE);
		$telefono = $this->input->post('phone',TRUE);
		$correo = $this->input->post('email',TRUE);
		$asunto = $this->input->post('asunto',TRUE);
		$mensaje = $this->input->post('comment',TRUE);
		$tipo = $this->input->post('tipo',TRUE);
		$sendDataContact = array(
			'name' => $nombre,
			'phone' => $telefono,
			'email' => $correo,
			'asunto' => $asunto,
			'comment' => $mensaje,
			'tipo' => $tipo
			);
		$bandera = $this->contact_model->save($sendDataContact);
		log_message('debug','$bandera='.$bandera);
		if(!$bandera)
		{
			$send = array('error'=> 'error');
			$this->load->view('contact',$send);
		}
		else
		{
			$this->load->library('email');
			$configGmail = array(
 				'protocol' => 'smtp',
 				'smtp_host' => 'ssl://smtp.gmail.com',
 				'smtp_port' => 465,
 				'smtp_user' => 'ujass227@gmail.com',
 				'smtp_pass' => 'alesana12',
 				'mailtype' => 'html',
 				'charset' => 'utf-8',
 				'newline' => "\r\n"
 				);
			//$this->load->view('email_contact_template', $data_send);
			$this->email->initialize($configGmail); 
 			$this->email->from($correo);
 			$this->email->to('contacto@difusa.com.mx');
 			$this->email->subject('Contacto DIFUSA: '.$correo.'');
 			$this->email->message("Contacto Usuario: ".$nombre." | correo: ".$correo." | telefono: ".$telefono."<br><br>Asunto: ".$asunto."<br><br>Mensaje:<br>".$mensaje."");
 			$this->email->send();
			redirect(base_url());
		}
	}
}
?>