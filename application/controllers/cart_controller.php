<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
   		$this->load->library('lb_moneda');
   		$this->load->model('panel_model');
   		$this->load->model('compra_model');
	}
	function inicio()
	{
		$carrito = $this->cart->contents();
		$user = $this->session->userdata('id_client');
    $userBill = $this->panel_model->getUserBill($user);
    if($this->cart->total() >= 5000 && $this->cart->total() < 20000)
    {
      $this->session->set_userdata('descuento1',true);
    }
    else if($this->cart->total() >= 20000)
    {
      $this->session->set_userdata('descuento2',true);
    }
    else if($this->cart->total() == 0)
    {
      $this->session->set_userdata('compra',true);
    }
		$send = array(
        'carrito' => $carrito,
				'clientData' => $userBill[0]
			);
		$this->load->view('carro',$send);
	}

	function cancel()
	{
		$carrito = $this->cart->contents();
		$user = $this->session->userdata('id_client');
		$userBill = $this->panel_model->getUserBill($user);
		$send = array('carrito' => $carrito,
				'clientData' => $userBill[0],
				'cancel' => 'cancel'
			);
		$this->load->view('carro',$send);		
	}

	function eliminarCarrito() 
	{
		$carrito = $this->cart->contents();
		$send = array('carrito' => $carrito);
		$this->cart->destroy();
    redirect(base_url().'carrito',$send);
  }
    
  function eliminarProducto($id)
  {
   	$data = array(
  		'rowid' => $id,
   		'qty' => 0
   		);
   	$this->cart->update($data);
   	$this->session->set_flashdata('agregado', 'El producto fue eliminado correctamente');
   	$carrito = $this->cart->contents();
		$send = array('carrito' => $carrito);
   	redirect(base_url().'carrito',$send);
  }

  function success()
  {
   	$carrito = $this->cart->contents();
   	$user = $this->session->userdata('id_client');
		$divisaDestino = $this->session->userdata('destino_cambio');
    $arrayProductsCode = array();
    $arrayOrder = array();
    $arrayMail = array();
    $total = 0;
    $totalUSD = 0;
    $totalMXN = 0;
    $totalEUR = 0;
    $c = 0;
    foreach($carrito as $item)
    {
      $precio = $item['price'];
      $divisaActual = $item['currency'];
      if((strcmp($divisaDestino, $divisaActual)) !== 0)
      {
        if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
          $precio *= $this->session->userdata('pesoAEuro');
        if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
          $precio *= $this->session->userdata('pesoADolar');
        if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
          $precio *= $this->session->userdata('euroAPeso');
        if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
          $precio *= $this->session->userdata('euroADolar');
        if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
          $precio *= $this->session->userdata('dolarAPeso');
        if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
          $precio *= $this->session->userdata('dolarAEuro');
      }
      $precio = number_format($precio,2,'.','');
      $precio2 = $precio * $item['qty'];
      $arrayMail[$c] = array(
          'id_producto' => $item['options']['code'],
          'precio_unitario' => $precio,
          'precio_con_cantidad' => $precio2,
          'divisa' => $item['currency'],
          'cantidad' => $item['qty'],
          'nombre' => $item['name']
        );
      $total+=$precio2;
      $precioUnityUSD = $precio;
      $precioUnityMXN = $precio;
      $precioUnityEUR = $precio;
      if((strcmp('MXN',$divisaDestino)) === 0)
      {
        $precioUnityUSD *= $this->session->userdata('pesoADolar');
        $precioUnityEUR *= $this->session->userdata('pesoAEuro');
        $precioUnityMXN = number_format($precioUnityMXN,2,'.','');
        $precioUnityUSD = number_format($precioUnityUSD,2,'.','');
        $precioUnityEUR = number_format($precioUnityEUR,2,'.','');
        $totalUSD += ($precioUnityUSD * $item['qty']);
        $totalEUR += ($precioUnityEUR * $item['qty']);
        $totalMXN += ($precioUnityMXN * $item['qty']);
      }
      if((strcmp('USD',$divisaDestino)) === 0)
      {
        $precioUnityMXN *= $this->session->userdata('dolarAPeso');
        $precioUnityEUR *= $this->session->userdata('dolarAEuro');
        $precioUnityUSD = number_format($precioUnityUSD,2,'.','');
        $precioUnityMXN = number_format($precioUnityMXN,2,'.','');
        $precioUnityEUR = number_format($precioUnityEUR,2,'.','');
        $totalUSD += ($precioUnityUSD * $item['qty']);
        $totalEUR += ($precioUnityEUR * $item['qty']);
        $totalMXN += ($precioUnityMXN * $item['qty']);
      }
      if((strcmp('EUR',$divisaDestino)) === 0)
      {
        $precioUnityUSD *= $this->session->userdata('euroADolar');
        $precioUnityMXN *= $this->session->userdata('euroAPeso');
        $precioUnityEUR = number_format($precioUnityEUR,2,'.','');
        $precioUnityMXN = number_format($precioUnityMXN,2,'.','');
        $precioUnityUSD = number_format($precioUnityUSD,2,'.','');                
        $totalUSD += ($precioUnityUSD * $item['qty']);
        $totalEUR += ($precioUnityEUR * $item['qty']);
        $totalMXN += ($precioUnityMXN * $item['qty']);
      }
      $arrayOrder['order_total_'.$divisaDestino] = $total;
      $c += 1;
    }
    if($c == count($carrito))
    {
      if((strcmp('MXN',$divisaDestino)) !== 0)
        $arrayOrder['order_total_MXN'] = number_format($totalMXN,2,'.','');
      if((strcmp('USD',$divisaDestino)) !== 0)
        $arrayOrder['order_total_USD'] = number_format($totalUSD,2,'.','');             
      if((strcmp('EUR',$divisaDestino)) !== 0)
        $arrayOrder['order_total_EUR'] = number_format($totalEUR,2,'.','');
    }
    $arrayOrder['order_status'] = 'Efectuada';
    $arrayOrder['id_user'] = $this->session->userdata('id_client');
    $this->compra_model->insertBuy($arrayOrder);
    $idOrden = $this->compra_model->getMaxOrderId();
    $c = 0;
    foreach($carrito as $item)
    {
      $precio = $item['price'];
      $divisaActual = $item['currency'];
      if((strcmp($divisaDestino, $divisaActual)) !== 0)
      {
        if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
          $precio *= $this->session->userdata('pesoAEuro');
        if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
          $precio *= $this->session->userdata('pesoADolar');
        if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
          $precio *= $this->session->userdata('euroAPeso');
        if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
          $precio *= $this->session->userdata('euroADolar');
        if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
          $precio *= $this->session->userdata('dolarAPeso');
        if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
          $precio *= $this->session->userdata('dolarAEuro');
      }
      $precio = number_format($precio,2,'.','');
      $precioUnityUSD = $precio;
      $precioUnityMXN = $precio;
      $precioUnityEUR = $precio;
      if((strcmp('MXN',$divisaDestino)) === 0)
      {
        $precioUnityUSD *= $this->session->userdata('pesoADolar');
        $precioUnityEUR *= $this->session->userdata('pesoAEuro');
        $precioUnityMXN = number_format($precioUnityMXN,2,'.','');
        $precioUSD = ($precioUnityUSD * $item['qty']);
        $precioEUR = ($precioUnityEUR * $item['qty']);
        $precioMXN = ($precioUnityMXN * $item['qty']);
        $arrayProductsCode[$c] = array(
          'id_product' => $item['id'],
          'id_order' => $idOrden[0]['max'],
          'quantity' => $item['qty'],
          'price_unity_MXN' => $precioUnityMXN,
          'price_unity_EUR' => number_format($precioUnityEUR,2,'.',''),
          'price_unity_USD' => number_format($precioUnityUSD,2,'.',''),
          'price_total_MXN' => number_format($precioMXN,2,'.',''),
          'price_total_USD' => number_format($precioUSD,2,'.',''),
          'price_total_EUR' => number_format($precioEUR,2,'.','')
        );
      }
      if((strcmp('USD',$divisaDestino)) === 0)
      {
                $precioUnityMXN *= $this->session->userdata('dolarAPeso');
                $precioUnityEUR *= $this->session->userdata('dolarAEuro');
                $precioUnityUSD = number_format($precioUnityUSD,2,'.','');
                $precioUnityMXN = number_format($precioUnityMXN,2,'.','');
                $precioUnityEUR = number_format($precioUnityEUR,2,'.','');
                $precioMXN = ($precioUnityMXN * $item['qty']);
                $precioEUR = ($precioUnityEUR * $item['qty']);
                $precioUSD = ($precioUnityUSD * $item['qty']);
                $arrayProductsCode[$c] = array(
          'id_product' => $item['id'],
          'id_order' => $idOrden[0]['max'],
          'quantity' => $item['qty'],
          'price_unity_MXN' => $precioUnityMXN,
          'price_unity_EUR' => $precioUnityEUR,
          'price_unity_USD' => $precioUnityUSD,
          'price_total_MXN' => number_format($precioMXN,2,'.',''),
          'price_total_USD' => number_format($precioUSD,2,'.',''),
          'price_total_EUR' => number_format($precioEUR,2,'.','')
          );
            }
            if((strcmp('EUR',$divisaDestino)) === 0)
            {
                $precioUnityUSD *= $this->session->userdata('euroADolar');
                $precioUnityMXN *= $this->session->userdata('euroAPeso');
                $precioUnityEUR = number_format($precioUnityEUR,2,'.','');
                $precioMXN = ($precioUnityMXN * $item['qty']);
                $precioEUR = ($precioUnityEUR * $item['qty']);
                $precioUSD = ($precioUnityUSD * $item['qty']);
                $arrayProductsCode[$c] = array(
                  'id_product' => $item['id'],
                  'id_order' => $idOrden[0]['max'],
                  'quantity' => $item['qty'],
                  'price_unity_MXN' => number_format($precioUnityMXN,2,'.',''),
                  'price_unity_EUR' => $precioUnityEUR,
                  'price_unity_USD' => number_format($precioUnityUSD,2,'.',''),
                  'price_total_MXN' => number_format($precioMXN,2,'.',''),
                  'price_total_USD' => number_format($precioUSD,2,'.',''),
                  'price_total_EUR' => number_format($precioEUR,2,'.','')
                );
            }
            $c+=1;
        }
        $this->compra_model->insertDetails($arrayProductsCode);
        $arrayPayment = array(
            'id_order' => $idOrden[0]['max']
          );
        $this->compra_model->insertPayment($arrayPayment);
    $this->sendingOrderMail($carrito);
		$s = $this->session->userdata('origen');
		if($s == NULL)
		{
			$monedas = array(
				'origen' => 'Peso Mexicano',
				'destino_cambio' => 'MXN'
				);
			$this->session->set_userdata($monedas);
		}
    $lastOrder = $this->compra_model->getMaxOrderId();
		redirect(base_url().'order-details/'.$lastOrder[0]['max']);
    }

    public function sendingOrderMail($carrito)
    {
      $this->load->library('email');
      $configGmail = array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => 'difusastore@gmail.com',
        'smtp_pass' => 'alesana12',
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'newline' => "\r\n"
      );
      $total = 0;
      $cantidad = 0;
      $concatenaMensaje = '<center><h2>Buen dia '.$this->session->userdata('client_name').' '.$this->session->userdata('last_name_client').'</h2></center><br>';
      $concatenaMensaje .= '<style type="text/css"> .carro {
  font-size: 14px;
    border-collapse: collapse;
    width: 100%;
}

.carro th,.carro td {
text-align: left;
    padding-right: 20px;
    padding-left: 10px;
    padding-top: 17px;

}

.carro tr:nth-child(even)
{
  background-color: #f2f2f2
}

.carro th {
    background-color: #b90f0d;
    /*text-align: center;*/
    color: white;
}</style>';
      $concatenaMensaje .= 'Los datos de los productos que solicitaste son:<br><br><br>';
      $concatenaMensaje .= '<table class="carro">';
      $concatenaMensaje .= '<tr><th>Nombre</th><th>Código</th><th>Precio</th><th>Cantidad</th><th>Subtotal</th></tr>';
      foreach ($carrito as $key)
      {
        $total += $key['price'] * $key['qty'];
        $cantidad += $key['qty'];
        $concatenaMensaje .= '<tr><td>'.$key['name'].'</td>';
        $concatenaMensaje .= '<td>'.$key['options']['code'].'</td>';
        $concatenaMensaje .= '<td>'.number_format($key['price'],2,'.',',').' '.$key['currency'].'</td>';
        $concatenaMensaje .= '<td>'.$key['qty'].'</td>';
        $concatenaMensaje .= '<td>'.number_format($key['price']*$key['qty'],2,'.',',').' '.$key['currency'].'</td></tr>';
      }
      $concatenaMensaje .= '<td></td><td><b>Total: </b></td><td></td><td>'.$cantidad.'</td><td>'.number_format($total,2,'.',',').' '.$key['currency'].'</td>';
      $concatenaMensaje .= '</table><br><br>';
      $concatenaMensaje .= '<center>En caso de alguna duda o aclaración favor de enviar un correo a contacto@difusa.com.mx</center>';
      $this->email->initialize($configGmail); 
      $this->email->from('difusastore@gmail.com',"DIFUSA");
      $this->email->reply_to('contacto@difusa.com.mx','DIFUSA');
      $this->email->to($this->session->userdata('contact_mail'));
      $this->email->subject('Datos de compra');
      $this->email->message($concatenaMensaje);
      $this->email->send();
      log_message('debug','resultado del correo'.$this->email->print_debugger());
    }

    function enviaPayPal()
    {
      $carrito = $this->cart->contents();
      $user = $this->session->userdata('id_client');
      $userBill = $this->panel_model->getUserBill($user);
      $send = array(
        'carrito' => $carrito,
        'clientData' => $userBill[0]
      );
      $this->load->view('redirecciona_carro_view',$send);
    }
}