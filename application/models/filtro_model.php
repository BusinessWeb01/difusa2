<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Filtro_model extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}

	function resultadosFiltro($nombre,$marca,$pais,$presentacion,$subcategory,$inicio,$fin)
	{
		$where = "";
		$where .= "p.product_name like '$nombre' ";
		if(strcmp($pais,'%') === 0)
			$where .= "AND p.country_name_iso3 like '$pais' ";
		else
			$where .= "AND p.country_name_iso3 = '$pais' ";
		if(strcmp($marca,'%') === 0)
			$where .= "AND p.id_brand like '$marca' ";
		else
			$where .= "AND p.id_brand = '$marca' ";
		if(strcmp($presentacion,'%') === 0)
			$where .= "AND p.id_presentation like '$presentacion' ";
		else
			$where .= "AND p.id_presentation = '$presentacion'";
		$where .= "AND p.id_sub_category = '$subcategory' ";
		$where .= "AND p.id_currency = c.id_currency ";
		$where .= "AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.id_product,p.product_code,p.product_name,b.brand,p.price,c.currency_name,p.url_images,pre.presentation,p.id_presentation,p.iva_tax,p.id_category');
		$this->db->from('products p');
		$this->db->from('brands b');
		$this->db->from('currency c');
		$this->db->from('presentation pre');
		$this->db->where($where);
		$this->db->group_by('p.id_product');
		$this->db->limit($fin,$inicio);
		$query = $this->db->get();
		log_message('debug','query del filtro resultadosFiltro '.$this->db->last_query());
		return $query->result_array();
	}

	function resultadosFiltroTotal($nombre,$marca,$pais,$presentacion,$subcategory)
	{
		$where = "";
		$where .= "p.product_name like '$nombre' ";
		if(strcmp($pais,'%') === 0)
			$where .= "AND p.country_name_iso3 like '$pais' ";
		else
			$where .= "AND p.country_name_iso3 = '$pais' ";
		if(strcmp($marca,'%') === 0)
			$where .= "AND p.id_brand like '$marca' ";
		else
			$where .= "AND p.id_brand = '$marca' ";
		if(strcmp($presentacion,'%') === 0)
			$where .= "AND p.id_presentation like '$presentacion' ";
		else
			$where .= "AND p.id_presentation = '$presentacion'";
		$where .= "AND p.id_sub_category = '$subcategory'";
		$this->db->select('p.id_product,p.product_code,p.product_name,b.brand,p.price,c.currency_name,p.url_images');
		$this->db->from('products p');
		$this->db->from('brands b');
		$this->db->from('currency c');
		$this->db->where($where);
		$this->db->group_by('p.id_product');
		$query = $this->db->get();
		log_message('debug','query del filtro '.$this->db->last_query());
		return $query->num_rows();
	}
}