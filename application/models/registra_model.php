<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Registra_model extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database();
    }
 
    function registro($data)
    {
        $this->db->trans_begin();

        $this->db->insert('ops_clients',$data);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function registroFactura($fact)
    {
        $this->db->trans_begin();

        $this->db->select('max(id_client) as max');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        $id = $query->result_array();
        $fact['id_client'] = $id[0]['max'];
        $this->db->insert('ops_client_address_bill',$fact);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function getMails()
    {
        $this->db->select('contact_mail as mails');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        return $query->result_array();
    }

    function newUserSuscription()
    {
        $this->db->select('max(id_client) as max');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        $id= $query->result_array();
        $idUser = $id[0]['max'];
        return $idUser;
    }

    function insertNewSuscription($suscriptions)
    {
        $this->db->trans_begin();

        $this->db->insert_batch('ops_subscriptions_ops_userss',$suscriptions);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function getStates()
    {
        $this->db->select('*');
        $this->db->from('estados');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getMunicipal($idState)
    {
        $this->db->select('*');
        $this->db->from('municipios');
        $this->db->where('estado_id',$idState);
        $query = $this->db->get();
        log_message('debug','datos generados con ajax'.print_r($query->result_array(),TRUE));
        return $query->result_array();
    }

    function getLocality($idMun)
    {
        $this->db->select('*');
        $this->db->from('localidades');
        $this->db->where('municipio_id',$idMun);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getStateMail($estado)
    {
        $this->db->select('nombre_estado');
        $this->db->from('estados');
        $this->db->where('id',$estado);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getMunicipalMail($idMun)
    {
        $this->db->select('nombre_municipio');
        $this->db->from('municipios');
        $this->db->where('id',$idMun);
        $query = $this->db->get();
        log_message('debug','datos generados con ajax'.print_r($query->result_array(),TRUE));
        return $query->result_array();
    }

    function getLocalityMail($idLocality)
    {
        $this->db->select('nombre_localidad');
        $this->db->from('localidades');
        $this->db->where('id',$idLocality);
        $query = $this->db->get();
        return $query->result_array();
    }

}