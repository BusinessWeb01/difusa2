<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activar_model extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}

	function activar($idactivacion)
	{
		$data = array('activated' => 1);
		$this->db->trans_begin();

		$this->db->where('id_client',$idactivacion);
		$this->db->update('clients',$data);

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}
}