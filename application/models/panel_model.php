<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel_Model extends CI_Model
{
 	function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function get_orders($user)
    {
    	$query = "SELECT orden.*,payment.* FROM  ops_orders orden, ops_payment payment WHERE orden.id_user=$user AND orden.id_order=payment.id_order ";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }
    
    function getUserBill($user)
    {
        $this->db->where('id_client',$user);
        $query = $this->db->get('client_address_bill');
        return $query->result_array();
    }

    function getUserData($user)
    {   
    	$query = "SELECT cliente.* FROM ops_clients cliente where cliente.id_client = $user";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function getUserBillData($user)
    {
        $where =  "factura.id_client = $user";
        $this->db->select('factura.*');
        $this->db->from('client_address_bill factura');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getComments($user)
    {
    	$query = "SELECT coment.*, product.id_product, producto.product_name,producto.id_category from ops_commentary_products coment, ops_products_ops_commentary product,ops_products producto WHERE coment.id_client = $user AND product.id_commentary = coment.id_commentary AND product.id_product = producto.id_product";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function getRefund($user)
    {
        $query = "SELECT refund.*, orden.id_order from ops_refund refund, ops_orders orden WHERE orden.id_user = $user AND refund.id_order = orden.id_order;";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function getNewsUser($user)
    {
    	$query = "SELECT id_subscription as 'user_subscription' FROM ops_subscriptions_ops_userss where id_user = $user";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function getNews()
    {
    	$query = "SELECT * FROM ops_subscriptions_catalog";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }
    
    function getWishes($user)
    {
    	$query = "SELECT deseo.*,producto.*,marca.*,moneda.*,presentacion.* FROM ops_presentation presentacion, ops_currency moneda, ops_brands marca, ops_wish_list deseo, ops_products producto where deseo.id_user = $user and deseo.id_product = producto.id_product AND marca.id_brand = producto.id_brand AND producto.id_currency = moneda.id_currency AND presentacion.id_presentation = producto.id_presentation";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function getDetails($order,$user)
    {
    	$query = "SELECT orden.*,detalle.*, producto.id_product, producto.product_name, producto.price,producto.id_category, producto.product_code, denominacion.currency_name from ops_currency denominacion, ops_detail_order detalle, ops_products producto, ops_orders orden WHERE orden.id_user = $user AND detalle.id_order = orden.id_order AND detalle.id_order = $order and producto.id_product = detalle.id_product and producto.id_currency = denominacion.id_currency ";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function updateUser($personal,$bussiness,$user)
    {
        $this->db->trans_begin();

        $this->db->where('id_client',$user);
        $this->db->update('ops_clients',$personal);
        $this->db->where('id_client',$user);
        $this->db->update('ops_client_address_bill',$bussiness);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function updateSuscription($user,$insert)
    {
        $this->db->trans_begin();

        $this->db->where('id_user',$user);
        $this->db->delete('ops_subscriptions_ops_userss');
        $this->db->insert_batch('ops_subscriptions_ops_userss',$insert);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }
    
    function noSuscription($user)
    {
        $this->db->trans_begin();

        $this->db->where('id_user',$user);
        $this->db->delete('ops_subscriptions_ops_userss');

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function getLastClient()
    {
        $this->db->select('max(id_client) as max');
        $this->db->from('clients');

        $query = $this->db->get();

        return $query->result_array();
    }
}