<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Compra_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    function insertBuy($arrayBuy)
    {
    	$this->db->trans_begin();

    		$this->db->insert('orders',$arrayBuy);

    	if($this->db->trans_status() === FALSE)
    		$this->db->trans_rollback();
    	else
    		$this->db->trans_commit();
    }

    function getMaxOrderId()
    {
        $this->db->select('max(id_order) as max');
        $this->db->from('ops_orders');
        $query = $this->db->get();
        return $query->result_array();
    }

    function insertDetails($details)
    {
    	$this->db->trans_begin();

            $this->db->insert_batch('ops_detail_order',$details);

    	if($this->db->trans_status() === FALSE)
    		$this->db->trans_rollback();
    	else
    		$this->db->trans_commit();
    }

    function insertPayment($arrayPayment)
    {
    	$this->db->trans_begin();

    		$this->db->insert('payment',$arrayPayment);

    	if($this->db->trans_status() === FALSE)
    		$this->db->trans_rollback();
    	else
    		$this->db->trans_commit();
    }
}