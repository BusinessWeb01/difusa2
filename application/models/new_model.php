<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class New_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    public function getNewProducts()
    {
    	$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$where = 'b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation';
    	$this->db->where($where);
        $this->db->order_by('created_at','desc');
    	$this->db->limit(9);
    	$query = $this->db->get();
        log_message('debug','query de los ultimos productos '.$this->db->last_query());
    	return $query->result_array();
    }
}

?>