<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_insumos extends CI_Model {
 
    
    function __construct() 
    {
        $this->load->database();
    }

    function prod($id)
    {
        $query='SELECT p.id_presentation,p.id_product,p.product_name, p.product_code, b.brand, p.price, p.product_description, c.currency_name, p.before_price, p.url_images,p.url_techsheets ,s.sub_category,y.category,x.country_name,z.presentation, t.product_type FROM ops_product_types t, ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_countries x, ops_categories y, ops_presentation z  WHERE p.id_product = "'.$id.'"  AND p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND s.id_sub_category=p.id_sub_category AND p.country_name_iso3=x.country_name_iso3 AND p.id_presentation=z.id_presentation AND p.id_product_type=t.id_product_type AND p.id_category=s.id_category';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    
    /**/function getTotalProducts($subcategory)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('id_sub_category',$subcategory);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**/function getSubcategoryName($subcategory)
    {
        $this->db->select('sub_category');
        $this->db->from('sub_categories');
        $this->db->where('id_sub_category',$subcategory);
        $query = $this->db->get();
        return $query->row_array();
    }

    /**/function getProducts($subcategory,$inicio,$fin)
    {
        $where = "p.id_sub_category = '$subcategory' AND p.id_brand = b.id_brand AND p.id_currency = c.id_currency AND p.id_presentation = x.id_presentation";
        $this->db->select('p.id_product,p.product_name,p.product_code,b.brand,p.price,c.currency_name,p.before_price, p.url_images, x.presentation,x.id_presentation,p.iva_tax');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('currency c');
        $this->db->from('presentation x');
        $this->db->where($where);
        $this->db->limit($fin,$inicio);
        $query = $this->db->get();
        log_message('debug','query de los productos '.$this->db->last_query());
        return $query->result_array();
    }

    /**/function pais($subcategoria)
    {
        $where = "p.id_sub_category = '$subcategoria' AND p.country_name_iso3 = c.country_name_iso3";
        $this->db->select('c.country_name_iso3,c.country_name');
        $this->db->from('countries c');
        $this->db->from('products p');
        $this->db->where($where);
        $this->db->group_by('c.country_name_iso3');
        $query = $this->db->get();
        log_message('debug','query de los paises '.$this->db->last_query());
        return $query->result_array();
    }

    /**/function marca($subcategory)
    {
        $where = "p.id_sub_category = '$subcategory' AND p.id_brand = c.id_brand";
        $this->db->select('c.id_brand,c.brand');
        $this->db->from('brands c');
        $this->db->from('products p');
        $this->db->where($where);
        $this->db->group_by('c.id_brand');
        log_message('debug','query de marcas '.$this->db->last_query());
        $query = $this->db->get();
        return $query->result_array();
    }
    function presentacion($subcategory)
    {
        $where = "p.id_sub_category = '$subcategory' AND p.id_presentation = c.id_presentation";
        $this->db->select('c.id_presentation, c.presentation');
        $this->db->from('presentation c');
        $this->db->from('products p');
        $this->db->where($where);
        $this->db->group_by('c.id_presentation');
        //"WHERE s.id_category=p.id_category AND  c.id_presentation=p.id_presentation AND s.id_category="INS" ORDER BY c.presentation";
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function getPresentationBags()
    {
        $where = "presentation like '%Saco%'";
        $this->db->select('id_presentation');
        $this->db->from('presentation');
        $this->db->where($where);
        $query = $this->db->get();
        log_message('debug','datos de las presentaciones '.$this->db->last_query());
        return $query->result_array();
    }
    
    function insertWish($datos)
    {
        $this->db->insert('ops_wish_list',$datos);
    }
}