<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms_model extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}

	function get_news($slug = FALSE)
	{
			$query = $this->db->get_where('pages',array ('slug' => $slug));
			return $query->row_array();
	}
}