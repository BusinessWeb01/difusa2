<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getProductsPerName($parameter,$inicio,$fin)
	{
		$where = "p.product_name LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

	public function getProductsPerCountry($parameter,$inicio,$fin)
	{
		$where = "con.country_name LIKE '".$parameter."' AND con.country_name_iso3 = p.country_name_iso3 AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
		$this->db->from('countries con');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

	public function getProductsPerBrand($parameter,$inicio,$fin)
	{
		$where = "b.brand LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

	public function getProductsPerType($parameter,$inicio,$fin)
	{
		$where = "typ.product_type LIKE '".$parameter."' AND typ.id_product_type = p.id_product_type AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');	
    	$this->db->from('currency cu');
    	$this->db->from('product_types typ');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

	public function getProductsPerCode($parameter,$inicio,$fin)
	{
		$where = "p.product_code LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

	public function getProductsPerSub($parameter,$inicio,$fin)
	{
		$where = "suca.sub_category LIKE '".$parameter."' AND suca.id_sub_category = p.id_sub_category AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->from('sub_categories suca');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function getProductsPerPresentation($parameter,$inicio,$fin)
	{
		$where = "pre.presentation LIKE '".$parameter."' AND pre.id_presentation = p.id_presentation AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND	p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
		$this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
    	$this->db->from('products p');
    	$this->db->from('brands b');
    	$this->db->from('categories c');
    	$this->db->from('currency cu');
    	$this->db->from('presentation pre');
    	$this->db->where($where);
    	$this->db->group_by('p.id_product');
        $this->db->limit($fin,$inicio);
    	$query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
    	return $query->result_array();
	}

    public function getProductsPerNameTotal($parameter)
    {
        $where = "p.product_name LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND  p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerCountryTotal($parameter)
    {
        $where = "con.country_name LIKE '".$parameter."' AND con.country_name_iso3 = p.country_name_iso3 AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('countries con');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerBrandTotal($parameter)
    {
        $where = "b.brand LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerTypeTotal($parameter)
    {
        $where = "typ.product_type LIKE '".$parameter."' AND typ.id_product_type = p.id_product_type AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND    p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');    
        $this->db->from('currency cu');
        $this->db->from('product_types typ');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerCodeTotal($parameter)
    {
        $where = "p.product_code LIKE '".$parameter."' AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND  p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerSubTotal($parameter)
    {
        $where = "suca.sub_category LIKE '".$parameter."' AND suca.id_sub_category = p.id_sub_category AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND  p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->from('sub_categories suca');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }

    public function getProductsPerPresentationTotal($parameter)
    {
        $where = "pre.presentation LIKE '".$parameter."' AND pre.id_presentation = p.id_presentation AND b.id_brand = p.id_brand AND p.id_category = c.id_category AND    p.id_currency = cu.id_currency AND p.id_presentation = pre.id_presentation";
        $this->db->select('p.*,b.brand,c.category,cu.currency_name,pre.presentation');
        $this->db->from('products p');
        $this->db->from('brands b');
        $this->db->from('categories c');
        $this->db->from('currency cu');
        $this->db->from('presentation pre');
        $this->db->where($where);
        $this->db->group_by('p.id_product');
        $query = $this->db->get();
        log_message('debug','Datos de la busqueda'.$this->db->last_query());
        return $query->num_rows();
    }
}