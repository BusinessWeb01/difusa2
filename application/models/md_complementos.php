<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_complementos extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function marca()
    {
        $where = "s.id_category = p.id_category AND c.id_brand=p.id_brand AND s.id_category='COM' GROUP BY c.id_brand";
        $this->db->select('c.id_brand,c.brand');
        $this->db->from('brands c');
        $this->db->from('categories s');
        $this->db->from('products p');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function insertWish($wishInsert)
    {
        $this->db->trans_begin();

        $this->db->insert('ops_wish_list',$wishInsert);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }

    function getPresentationBags()
    {
        $where = "presentation like '%Saco%'";
        $this->db->select('id_presentation');
        $this->db->from('presentation');
        $this->db->where($where);
        $query = $this->db->get();
        log_message('debug','datos de las presentaciones '.$this->db->last_query());
        return $query->result_array();
    }
}