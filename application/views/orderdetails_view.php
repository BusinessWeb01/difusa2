<div>
<br><br><br>
<legend>Desglose de pedido</legend>
<table class='rowstable'>
	<tr><th>Producto</th><th>Referencia de producto</th><th>Precio</th><th>Cantidad</th><th>Subtotal</th></tr>
	<?php
		$denominacion = $this->session->userdata('destino_cambio');
		$total = 0;
		for($i=0;$i<count($detail);$i++)
		{
			echo "<tr>";
			if(strcmp($detail[$i]['id_category'],"INS") === 0)
				echo "<td><a href='".base_url()."info-insumo/".$detail[$i]['id_product']."#visualizar'>".$detail[$i]['product_name']."</a></td>";
			elseif(strcmp($detail[$i]['id_category'],"COM") === 0 )
				echo "<td><a href='".base_url()."info-complemento/".$detail[$i]['id_product']."#visualizar'>".$detail[$i]['product_name']."</a></td>";
			echo "<td id='ids'>".$detail[$i]['product_code']."</td>";
			if(strcmp('USD',$denominacion) === 0)
			{
				echo "<td> $".number_format($detail[$i]['price_unity_USD'],2,'.',',')." "	.$detail[$i]['currency_name']."</td>";
				echo "<td>".$detail[$i]['quantity']."</td>";
				$precio = number_format($detail[$i]['price_total_USD'],2,'.',',');
				echo "<td id='total'> $". $precio ."</td>";
				echo "</tr>";
			}
			if(strcmp('MXN',$denominacion) === 0)
			{
				echo "<td> $".number_format($detail[$i]['price_unity_MXN'],2,'.',',')." ".$denominacion."</td>";
				echo "<td>".$detail[$i]['quantity']."</td>";
				$precio = number_format($detail[$i]['price_total_MXN'],2,'.',',');
				echo "<td id='total'> $". $precio ."</td>";
				echo "</tr>";
			}
			if(strcmp('EUR',$denominacion) === 0)
			{
				echo "<td> $".number_format($detail[$i]['price_unity_EUR'],2,'.',',')." ".$denominacion."</td>";
				echo "<td>".$detail[$i]['quantity']."</td>";
				$precio = number_format($detail[$i]['price_total_EUR'],2,'.',',');
				echo "<td id='total'> $". $precio ."</td>";
				echo "</tr>";
			}
		}
		if(strcmp('USD',$denominacion) === 0)
		{
		?>  
			<tr><td></td><td></td><td></td><td></td><td style="text-align:right;">Total&nbsp; <?php echo number_format($detail[0]['order_total_USD'],2,'.',',') ?></td></tr>
		<?php 
		}
		if(strcmp('MXN',$denominacion) === 0)
		{
		?>  
			<tr><td></td><td></td><td></td><td></td><td style="text-align:right;">Total&nbsp; <?php echo number_format($detail[0]['order_total_MXN'],2,'.',',') ?></td></tr>
		<?php 
		} 
		if(strcmp('EUR',$denominacion) === 0)
		{
		?>  
			<tr><td></td><td></td><td></td><td></td><td style="text-align:right;">Total&nbsp; <?php echo number_format($detail[0]['order_total_EUR'],2,'.',',') ?></td></tr>
		<?php 
		}
		?>
</table><br><br>
<button style="float: right;" type="button" name="Regresa" class="btn btn-danger btn-sm" onclick="history.back();">Regresar</button>
</div>
</div>
</div>
</div>
</div>
<br><br><br><br><br>
<?php get_footer();?>