<?php get_header();?>
<br /><br />
<div class="container animated zoomIn" id="contenedor">
<legend><h2>Recupera tu contraseña</h2></legend>
<div class="col-xs-4">
	<div class="form-group">
		<form name="form_register" onsubmit="transicion(); return verifyNewPass();" method="post" action="<?php echo base_url();?>recuperar" autocomplete="off"> 
        <div class="row">
                <label>Código de confirmación:</label>
                <input type="text" class="form-control control-contact" id="code" name="code" placeholder="Código" autocomplete="off" required>
            </div>
			<div class="row">
				<label>Nueva contraseña:</label>
				<input type="text" class="form-control control-contact" id="pass1" name="pass1" placeholder="Contraseña" autocomplete="off" required>
			</div>
            <div class="row">
                <label>Confirma tu contraseña:</label>
                <input type="text" class="form-control control-contact" id="pass2" name="pass2" autocomplete="off" required>
            </div>
			<br><br><div class="colmd-12 col-md-offset-0"><button type="submit" name="enviar" id="enviar" class="btn btn-danger" onclick="">Reestablecer contraseña</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
            <input type="hidden" name="correo" value="<?php echo $correo;?>">
            <input type="hidden" name="codigo" value="<?php echo $codigo;?>" id="codigo">
		</form>
	</div>
</div>
</div><br><br><br><br><br><br><br><br><br><br><br><br>
<?php get_footer();?>