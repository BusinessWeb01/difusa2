<?php get_header();?>
<br>
<div class="container">
<br><br><br>
	<?php
		if(isset($top))
		{
			?>
			<center><legend style="font-size: 40px;" >Top Sales</legend></center>
			<br><br><br>
			<?php
			foreach($codigos as $producto)
			{
				for($i=0;$i<count($producto);$i++)
				{
					if($this->session->userdata('id_client'))
					{
						if(strcmp($producto[$i]['id_category'],'INS'))
						{
						?>
						<a href="<?php echo base_url()."info-insumo/".$producto[$i]['id_product'];?>#visualizar">
							<div class="form-group">
						<?php
						}
						else
						{
						?>
						<a href="<?php echo base_url()."info-complemento/".$producto[$i]['id_product'];?>#visualizar">
							<div class="form-group">
						<?php
						}
					}
					else
					{
						?>
							<a href="<?php echo base_url()."info-producto/".$producto[$i]['id_product'];?>#visualizar">
								<div class="form-group">
						<?php
					}
					?>
							<div class="boxpr3 col-xs-8	">
								<img class="imagpr" src="<?php echo base_url();echo $producto[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
								<br><br>
								Nombre:&nbsp;
								<?php
									echo $producto[$i]['product_name']."<br>";
								?>
								C&oacute;digo:&nbsp;
								<?php
									echo $producto[$i]['product_code']."<br>";
								?>
								Marca:&nbsp;
								<?php
									echo $producto[$i]['brand']."<br>";
								?>
								Precio:&nbsp;
								<?php
									echo $producto[$i]['price']." ";
								?>
								<?php
									echo $producto[$i]['currency_name']."<br>";
								?>
								Presentaci&oacute;n&nbsp;
								<?php
									echo $producto[$i]['presentation']."<br>";
								?>
							</div>
						</div>
					</a>
				<?php
				}
			}
		}
		elseif($new)
		{
			?>
			<center><legend style="font-size: 40px;" >Nuevos Productos</legend></center>
			<br><br><br>
			<?php
			//print_r($codigos);
				for($i=0;$i<count($codigos);$i++)
				{
					if($this->session->userdata('id_client'))
					{
						if(strcmp($codigos[$i]['id_category'],'INS'))
						{
						?>
						<a href="<?php echo base_url()."info-insumo/".$codigos[$i]['id_product'];?>#visualizar">
							<div class="form-group">
						<?php
						}
						else
						{
						?>
						<a href="<?php echo base_url()."info-complemento/".$codigos[$i]['id_product'];?>#visualizar">
							<div class="form-group">
						<?php
						}
					}
					else
					{
						?>
							<a href="<?php echo base_url()."info-producto/".$codigos[$i]['id_product'];?>#visualizar">
								<div class="form-group">
						<?php
					}
					?>
							<div class="boxpr3 col-xs-8	">
								<img class="imagpr" src="<?php echo base_url();echo $codigos[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
								<br><br>
								Nombre:&nbsp;
								<?php
									echo $codigos[$i]['product_name']."<br>";
								?>
								C&oacute;digo:&nbsp;
								<?php
									echo $codigos[$i]['product_code']."<br>";
								?>
								Marca:&nbsp;
								<?php
									echo $codigos[$i]['brand']."<br>";
								?>
								Precio:&nbsp;
								<?php
									echo number_format($codigos[$i]['price'],2,'.',',')." ".$codigos[$i]['currency_name']."<br>";
								?>
								Presentaci&oacute;n&nbsp;
								<?php
									echo $codigos[$i]['presentation']."<br>";
								?>
							</div>
						</div>
					</a>
				<?php
				}
			}	
	?>
	</div><br><br>
<?php get_footer();?>