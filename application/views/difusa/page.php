<?php get_header(); ?>
<br /><br />
<?php load_view('section_title', array('text' => $title)); ?>
<br /><br /><br />
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<br /><br /><br />
<?php get_footer(); ?>