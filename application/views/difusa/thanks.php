<?php get_header(); ?>
<?php
 echo ''; 
 /* 
 load_view('carousel_top', $carousel_config);*/

    $contenido_carrito = $carrito['session_user']['cart_contents'];


?>
<br /><br />
<?php load_view('section_title', array('text' => 'Agradecemos su Confianza')); ?>
<br /><br />
<style>
    #resumen {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        width: 50%;
        border-collapse: collapse;
        /*align: "center";*/
        margin-left: auto;
        margin-right: auto;
    }

    #resumen td, #resumen th {
        font-size: 1em;
        border: 1px solid #F7BEA0;
        padding: 3px 7px 2px 7px;
    }

    #resumen th {
        font-size: 1.1em;
        text-align: left;
        padding-top: 5px;
        padding-bottom: 4px;
        background-color: #F7BEA0;
        color: #ffffff;
    }

    #resumen tr.alt td {
        color: #000000;
        background-color: #FBDBCA;
    }

    @media print {
      a[href]:after {
        content: none !important;
      }
    }
</style>

<div style="text-align:center;">
    <h2> Resumen de su compra</h2>


<?php if($get['f']&&$desplegarPantallaExito=='si'){   ?>
 <table id="resumen">
    <tr >
    <td>Mensaje:</td><td><?php echo "Gracias por su pago vía PayPal."; ?></td>
    </tr>
    <tr class="alt">
    <td>Folio:</td><td><?php echo $get['f'] ?></td>
    </tr>
    <tr>
    <td>Cadena:</td><td><?php echo $get['t'] ?></td>
    </tr>
    <tr class="alt">
        <td>Su pedido:</td><td><?php echo ''; ?></td>
    </tr>
    <?php
    foreach($contenido_carrito as $key => $value){
                //echo $key.' -- '.$value.'</br>';
                if(is_array($value)){
            /*        echo $value['name'].'</br>';
                    echo $value['options']['model'].'</br>';
                    echo $value['options']['color'].'</br>';
                    echo $value['options']['kilate'].'</br>';
                    echo $value['options']['point'].'</br>';
                    echo $value['options']['measure'].'</br>';
            */
    ?>

    <tr>
        <td>Producto</td><td><?php echo 'Nombre: '.$value['name'].' '.'Modelo: '.$value['options']['model'].', Caracteristicas:'.$value['options']['color'].', '.$value['options']['kilate'].',  Puntos: '.$value['options']['point'].',  Medida: '.$value['options']['measure']; ?></td>
    </tr>

    <?php
        }}
    ?>


 </table>

<?php } else if($post['tarjetahabiente']&&$desplegarPantallaExito=='si') { ?>

<table id="resumen">
    <tr>
    <td>Tarjetabiente:</td><td> <?php echo $post['tarjetahabiente'] ?></td>
    </tr>

    <tr class="alt">
        <td>C&oacute;digo:</td><td><?php echo $post['codigo'] ?></td>
    </tr>
    <tr>
    <td>Mensaje:</td><td><?php echo $post['mensaje'] ?></td>
    </tr>
    <tr class="alt">
        <td>Autorizaci&oacute;n:</td><td><?php echo $post['autorizacion'] ?></td>
    </tr>
    <tr>
    <td>Referencia:</td><td><?php echo $post['referencia'] ?></td>
    </tr>
    <tr class="alt">
    <td >Importe:</td><td><?php echo $post['importe'] ?></td>
    </tr>
    <tr>
    <td>Medio de pago:</td><td><?php echo $post['mediopago'] ?></td>
    </tr>
    <tr class="alt">
    <td>Financiado:</td><td><?php echo $post['financiado'] ?></td>
    </tr>
    <tr>
    <td>Plazos:</td><td><?php echo $post['plazos'] ?></td>
    </tr>
    <tr class="alt">
        <td>Transmisi&oacute;n:</td><td><?php echo $post['s_transm'] ?></td>
    </tr>
    <tr>
    <td>Cadena:</td><td><?php echo $post['hash'] ?></td>
    </tr>

    <tr class="alt">
        <td>Su pedido:</td><td><?php echo ''; ?></td>
    </tr>
    <?php
    foreach($contenido_carrito as $key => $value){
                //echo $key.' -- '.$value.'</br>';
                if(is_array($value)){
            /*        echo $value['name'].'</br>';
                    echo $value['options']['model'].'</br>';
                    echo $value['options']['color'].'</br>';
                    echo $value['options']['kilate'].'</br>';
                    echo $value['options']['point'].'</br>';
                    echo $value['options']['measure'].'</br>';
            */
    ?>

    <tr>
        <td>Producto</td><td><?php echo 'Nombre: '.$value['name'].' '.'Modelo: '.$value['options']['model'].', Caracteristicas:'.$value['options']['color'].', '.$value['options']['kilate'].',  Puntos: '.$value['options']['point'].',  Medida: '.$value['options']['measure']; ?></td>
    </tr>

    <?php
        }}
    ?>


    
</table>

<?php } ?>

    
    <br /><br />
    <a href="javascript:window.print()">Imprimir esta página</a>
</div>
    <br /><br />
    <br /><br />
    <br /><br />
