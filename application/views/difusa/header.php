<?php get_header('top');?>
<header>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0 padding-for-logo-header text-center">
                    <a href="<?php echo base_url(); ?>" class="logo-header-link">
                        <img src="<?php echo base_url(); ?>content/themes/difusa/img/logo-web.png" alt="Logo de Difusa" />
                    </a>
                </div> 
                <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                    <h1 class="text-right texto-header-contact margen-top-texto-header hidden-xs">
                    	<a href="#moneda" class="link-to-cart">
                    		Moneda&nbsp;<?php echo $this->session->userdata('destino_cambio');?>
  						</a>
						/
						<?php if(!$this->session->userdata('id_client') && !$this->session->userdata('username')){?>
						<a href="<?php echo base_url(); ?>login" class="link-to-cart"><span class="glyphicon glyphicon-log-in">
							</span>	Iniciar Sesión
						</a>
						/
						<a href="<?php echo base_url(); ?>register" class="link-to-cart">
							<span class="glyphicon glyphicon-ok">
							</span>	Registrarse
						</a>
						<?php }elseif($this->session->userdata('id_client')){
							?>
							<a href="<?php echo base_url(); ?>carrito" class="link-to-cart">
							<span class="glyphicon glyphicon-shopping-cart">
							</span>Carrito (<?php echo $this->cart->total_items(); ?>)
						</a>
						/
						<a href="<?php echo base_url(); ?>user-info" class="link-to-cart">
							<span class="glyphicon glyphicon-user">
							</span>	<?php echo $this->session->userdata('client_name')." ".$this->session->userdata('last_name_client')?>
						</a>
						/
						<a href="<?php echo base_url(); ?>logout" class="link-to-cart">
							<span class="glyphicon glyphicon-off">
							</span>	Logout
						</a>
						<?php }elseif($this->session->userdata('username')){?>
						<a href="<?php echo base_url(); ?>clientes" class="link-to-cart">
							<span class="glyphicon glyphicon-user">
							</span>&nbsp;Admin <?php echo $this->session->userdata('username')?>
						</a>
						/
						<a href="<?php echo base_url(); ?>logout" class="link-to-cart">
							<span class="glyphicon glyphicon-off">
							</span>	Logout
						</a>
						<?php }?>
						<br/>
						<br/>
                        <a href="<?php echo base_url(); ?>contacting">(55) 5293-1292</a><br />
                        <a href="<?php echo base_url(); ?>contacting">contacto@difusa.com.mx</a><br />
						
						<div class="col-md-12 col-md-offset-0 col-sm-8 col-sm-offset-12 col-xs-8 col-xs-offset-2">
							<form action="<?php echo base_url(); ?>search/1" accept-charset="utf-8" id="form_search" method="post" enctype="multipart/form-data" class="form-inline text-right">
								<div class="form-group margen-top-busq-header">
										<input type="image" src="<?php echo base_url(); ?>content/themes/difusa/img/lupa.png" alt="lupa" class="hidden-xs"/>
									<select name="opcion" class="form-control input-sm altura-input-busqueda2 ">
										<option value="nombre">Nombre</option>
										<option value="codigo">Código</option>
										<option value="marca">Marca</option>
										<option value="subcategoria">Subcategoria</option>
										<option value="presentacion">Presentacion</option>
										<option value="tipo">Tipo</option>
										<option value="pais">Pais</option>
									</select>
									<input type="text" class="form-control input-sm altura-input-busqueda" name="parametro" />
									<div class="row visible-xs">
										<div class="col-xs-12 col-xs-offset-0 text-center">
											<a class="btn_img_searc" href="#">
												<img src="<?php echo base_url(); ?>content/themes/difusa/img/lupa.png" alt="lupa" class="btn_img_searc" />
											</a>
										</div>
									</div>
								</div>
							</form>
						</div><br>
						<br/><br/>
					</h1>
                    <h1 class="text-center texto-header-contact margen-top-texto-header visible-xs">
                        <br />
                        <a href="#moneda" class="link-to-cart">
    						Moneda&nbsp;<?php echo $this->session->userdata('destino_cambio');?>
  						</a>
						/
						<?php if(!$this->session->userdata('id_client') && !$this->session->userdata('username')){?>
						<a href="<?php echo base_url(); ?>login" class="link-to-cart">
							<span class="glyphicon glyphicon-log-in">
							</span>	Iniciar Sesión
						</a>
						/
						<a href="<?php echo base_url(); ?>register" class="link-to-cart">
							<span class="glyphicon glyphicon-ok">
							</span>	Registrarse
						</a>						
						<?php }elseif($this->session->userdata('id_client')){
							?>
							<a href="<?php echo base_url(); ?>carrito" class="link-to-cart">
							<span class="glyphicon glyphicon-shopping-cart">
							</span>	Carrito (<?php echo $this->cart->total_items(); ?>)
						</a>
						/
						<a href="<?php echo base_url(); ?>user-info" class="link-to-cart">
							<span class="glyphicon glyphicon-user">
							</span>	<?php echo $this->session->userdata('client_name')." ".$this->session->userdata('last_name_client')?>
						</a>
						/
						<a href="<?php echo base_url(); ?>logout" class="link-to-cart">
							<span class="glyphicon glyphicon-off">
							</span>	Logout
						</a>
						<?php }elseif($this->session->userdata('username')){?>
						<a href="<?php echo base_url(); ?>clientes" class="link-to-cart"><span class="glyphicon glyphicon-log-in">
							</span>&nbsp;Admin 
							<?php echo $this->session->userdata('username')?>
						</a>
						/
						<a href="<?php echo base_url(); ?>logout" class="link-to-cart">
							<span class="glyphicon glyphicon-off">
							</span>	Logout
						</a>
						<?php }?>
						<br/>
						<br/>
                        <a href="<?php echo base_url(); ?>contacting">(55) 5293-1292</a><br />
                        <a href="<?php echo base_url(); ?>contacting">contacto@difusa.com.mx</a><br />
						<div class="col-md-3 col-md-offset-0 col-sm-8 col-sm-offset-3 col-xs-8 col-xs-offset-2">
							<form action="<?php echo base_url(); ?>search/1" accept-charset="utf-8" id="form_search" method="post" enctype="multipart/form-data" class="form-inline text-right">
								<div class="form-group margen-top-busq-header">
										<input type="image" src="<?php echo base_url(); ?>content/themes/difusa/img/lupa.png" alt="lupa" class="hidden-xs"/>
									<select name="opcion" class="form-control input-sm altura-input-busqueda">
										<option value="nombre">Nombre</option>
										<option value="codigo">Código</option>
										<option value="marca">Marca</option>
										<option value="subcategoria">Subcategoria</option>
										<option value="presentacion">Presentacion</option>
										<option value="tipo">Tipo</option>
										<option value="pais">Pais</option>
									</select>
									<input type="text" class="form-control input-sm altura-input-busqueda" name="parametro" />
									<div class="row visible-xs">
										<div class="col-xs-12 col-xs-offset-0 text-center">
											<a class="btn_img_searc" href="#">
												<img src="<?php echo base_url(); ?>content/themes/difusa/img/lupa.png" alt="lupa" class="btn_img_searc" />
											</a>
										</div>
									</div>
								</div>
							</form>
						</div><br>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="hidden-xs">
    <div class="row color-difusa-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
					<nav class="menu_gral">
                    <ul class="nav nav-pills nav-top-header nav navbar-nav">
						<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'ingredientes'){ echo 'active'; } } ?>">
							<!--menu-->
							<?php if(!$this->session->userdata('id_client')){?>
							<a>
								Insumos
							</a>
							<ul class="nav">
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos/LUP/1">Lúpulo</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos/LVL/1">Levadura Líquida</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos/LVS/1">Levadura Seca</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos/MAL/1">Malta</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos/NUL/1">Nutriente de Levadura</a>
								</li>
							</ul>
							<?php }else{?>
							<a>Insumos</a>
							<ul class="nav">
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos-user/LUP/1">Lúpulo</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos-user/LVL/1">Levadura Líquida</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos-user/LVS/1">Levadura Seca</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos-user/MAL/1">Malta</a>
								</li>
								<li>
									<a class="links" href="<?php echo base_url()?>Insumos-user/NUL/1">Nutriente de Levadura</a>
								</li>
							</ul>
							<?php }?>
						</li>
                        <?php if(!$this->session->userdata('id_client')){?>
                        	<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/coadyudantes'){ echo 'active'; } } ?>">
                        	<a>Complementos</a>
                        	<ul class="nav">
                        		<li>
                        			<a class="links" href="<?php echo base_url()?>Complementos/COA/1">Coadyuvantes</a>
                        		</li>
                        		<li>
                        			<a class="links" href="<?php echo base_url()?>Complementos/KEG/1">Keg's</a>
                        		</li>
                        		<li>
                        			<a class="links" href="<?php echo base_url()?>Complementos/KIT/1">Kits Sensoriales</a>
                        		</li>
                        	</ul>
                        	</li>
                        	<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/equipo'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>team-contacting">Equipo</a></li>
                        <li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'contacto'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>contacting">Contacto</a></li>
						<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>promociones">Promociones</a></li>
                        	<?php }else{?>
                        		<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/coadyudantes'){ echo 'active'; } } ?>">
                        			<a>Complementos</a>
                        			<ul class="nav">
                        				<li>
                        					<a class="links" href="<?php echo base_url()?>Complementos-user/COA/1">Coadyuvantes</a>
                    	    			</li>
                	        			<li>
	        	                			<a class="links" href="<?php echo base_url()?>Complementos-user/KEG/1">Keg's</a>
        	                			</li>
    	                    			<li>
		                        			<a class="links" href="<?php echo base_url()?>Complementos-user/KIT/1">Kits Sensoriales</a>
                        				</li>
                        			</ul>
                        		</li>
                        		<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/equipo'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>team-contacting">Equipo</a></li>
                        <li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'contacto'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>contacting">Contacto</a></li>
						<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'search-products/promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>promociones">Promociones</a></li>
								<?php }?>
						</ul>
					</nav>
                </div>                
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-default visible-xs navbar-header-xs">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#btn_navbar_header_xs">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>">DIFUSA</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="btn_navbar_header_xs">
            <ul class="nav navbar-nav" style="color:white;">
             <?php if(!$this->session->userdata('id_client')){?>
                <li class="<?php if(isset($section_nav)){if($section_nav == 'home'){ echo 'active'; } } ?>">
                	<a>Insumos</a>
                	<ul class="nav">
						<li>
							<a class="links" href="<?php echo base_url()?>Insumos/LUP/1">Lúpulo</a>
						</li>
						<li>
							<a class="links" href="<?php echo base_url()?>Insumos/LVL/1">Levadura Líquida</a>
						</li>
						<li>
							<a class="links" href="<?php echo base_url()?>Insumos/LVS/1">Levadura Seca</a>
						</li>
						<li>
							<a class="links" href="<?php echo base_url()?>Insumos/MAL/1">Malta</a>
						</li>
						<li>
							<a class="links" href="<?php echo base_url()?>Insumos/NUL/1">Nutriente de Levadura</a>
						</li>
					</ul>
                </li>
                <li class="<?php if(isset($section_nav)){ if($section_nav == 'nosotros'){ echo 'active'; } } ?>">
				<a>Complementos</a>
				<ul class="nav">
                  	<li>
                    	<a class="links" href="<?php echo base_url()?>Complementos/COA/1">Coadyuvantes</a>
                    </li>
                    <li>
                    	<a class="links" href="<?php echo base_url()?>Complementos/KEG/1">Keg's</a>
                    </li>
					<li>
                       	<a class="links" href="<?php echo base_url()?>Complementos/KIT/1">Kits Sensoriales</a>
                   	</li>
                   	</ul>
                </li>
				<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>team-contacting">Equipo</a></li>
				<li class="<?php if(isset($section_nav)){ if($section_nav == 'contacto'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>contacting">Contacto</a></li>
                <li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>promociones">Promociones</a></li>
				<?php }else{?>
					<li class="<?php if(isset($section_nav)){ if($section_nav == 'home'){ echo 'active'; } } ?>">
                		<a>Insumos</a>
                		<ul class="nav">
							<li>
								<a class="links" href="<?php echo base_url()?>Insumos-user/LUP">Lúpulo</a>
							</li>
							<li>
								<a class="links" href="<?php echo base_url()?>Insumos-user/LVL">Levadura Líquida</a>
							</li>
							<li>
								<a class="links" href="<?php echo base_url()?>Insumos-user/LVS">Levadura Seca</a>
							</li>
							<li>
								<a class="links" href="<?php echo base_url()?>Insumos-user/MAL">Malta</a>
							</li>
							<li>
								<a class="links" href="<?php echo base_url()?>Insumos-user/NUL">Nutriente de Levadura</a>
							</li>
						</ul>
                	</li>
                <li class="<?php if(isset($section_nav)){ if($section_nav == 'nosotros'){ echo 'active'; } } ?>">
					<a>Complementos</a>
					<ul class="nav">
                        <li>
                        	<a class="links" href="<?php echo base_url()?>Complementos-user/COA/1">Coadyuvantes</a>
                    	</li>
                	    <li>
	        	        	<a class="links" href="<?php echo base_url()?>Complementos-user/KEG/1">Keg's</a>
        	            </li>
    	          		<li>
		                	<a class="links" href="<?php echo base_url()?>Complementos-user/KIT/1">Kits Sensoriales</a>
                        </li>
                    </ul>
				</li>
				<li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>team-contacting">Equipo</a></li>
				<li class="<?php if(isset($section_nav)){ if($section_nav == 'contacto'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>contacting">Contacto</a></li>
                <li role="presentation" class="<?php if(isset($section_nav)){ if($section_nav == 'promociones'){ echo 'active'; } } ?>"><a href="<?php echo base_url(); ?>promociones">Promociones</a></li>
				<?php }?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="overlay" id="moneda" style="z-index:1;">
  <div class="popup moneda">
    <h3>Cambio de divisa</h3><br>
    <a class="close" href="#">&times;</a>
    <div class="content">
    	<form action="<?php echo base_url();?>moneda_controller/changueCurrency"  method="post">
     		<p class="monedastyle">Divisa actual:&nbsp;&nbsp;
     		<input type="text" name="moneda_origen" size="15" value="<?php echo $this->session->userdata('origen')?>" maxlength="15" readonly/><br></p>
     		<br><br>
      		<p class="monedastyle">Divisa a elegir:&nbsp;&nbsp;
     		<select name="moneda_destino">
     			<?php 
     				switch($this->session->userdata('origen'))
     				{
     					case "Peso Mexicano":
     						?>
     							<option value="USD">Dolar Americano</option>
     						<?php
     						break;
     					case "Dolar Americano":
     						?>
     							<option value="MXN">Peso Mexicano</option>
     						<?php
     						break;
     				}
     			?>     			
     		</select></p>
      		<br><br><button type="submit" class="btn btn-sm btn-info" style="text-align:center;">Cambiar</button>
      	</form>
  	</div>
  </div>
</div>