<?php get_header(); ?>
<br /><br />
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <img src="<?php echo get_option('path_template'); ?>img/404.png" alt="Error 404" class="img-responsive" />
        </div>
    </div>
</div>
<br /><br />
<?php get_footer('bottom'); ?>