<?php if(isset($universal_prices)): ?>
<script>
    //ndp 20150722 - original
    function get_price_with_universal_prices(kilate, punto) {
        var universal_prices = JSON.parse('<?php echo $universal_prices; ?>');
        var k = 'k'.concat(kilate);
        var p = 'p'.concat(punto);
        var price = 0;
        //ndp 20150722 -
        //alert("k="+k);
        //alert("p="+p);
        if (k in universal_prices) {
            if (p in universal_prices[k]) {
                price = universal_prices[k][p];
            }
        }//ndp 20150722 -
        if(price==0){
            //alert("price = 0,  k="+k+" p="+p);

            //$('#kilate_14K').hide();
            //$('#kilate_14K').toggle();
           // $('#kilate_18K').attr("readonly", "readonly");
            //$('#kilate_'+k).hide();

            //alert('#kilate_'+k);

        }


        
        return price;
    }
    //20150722 - modificada
    /*
    function get_price_with_universal_prices2(kilate, punto, kilateDefault, puntoDefault) {
        var universal_prices = JSON.parse('<?php echo $universal_prices; ?>');
        var k = 'k'.concat(kilate);
        var p = 'p'.concat(punto);
        var price = 0;
        //ndp 20150722 -
        alert("k="+k);
        alert("p="+p);
        if (k in universal_prices) {
            if (p in universal_prices[k]) {
                price = universal_prices[k][p];
            }
        }//ndp 20150722 -
        if(price==0){
            alert("price = 0,  k="+k+" p="+p);

            //$('#kilate_14K').hide();
            $('#kilate_14K').toggle();
            //$('#kilate_'+k).hide();

            //alert('#kilate_'+k);

        }
        return price;
    }*/
</script>
<?php endif; ?>

<?php if(isset($categories_rings)): ?>
<script>
$(document).ready(function(){
    <?php foreach($categories_rings as $value): ?>
        $("#cate_<?php echo $value->slug; ?>").hover(function() {
            var width_window = $( window ).width();
            if (width_window > 767) {
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "110px" );
                }
                $( "#i_n_<?php echo $value->slug; ?>" ).hide();
                $( "#d_<?php echo $value->slug; ?>" ).show();
                $( "#c_<?php echo $value->slug; ?>" ).addClass( "actual-rings-cate-des" );
            }
        },function() {
            var width_window = $( window ).width();
            if (width_window > 767){
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "150px" );
                }
                $( "#c_<?php echo $value->slug; ?>" ).removeClass( "actual-rings-cate-des" );
                $( "#i_n_<?php echo $value->slug; ?>" ).show();
                $( "#d_<?php echo $value->slug; ?>" ).hide();
            }
        });
    <?php endforeach; ?>
});
</script>
<?php endif; ?>

<?php if(isset($all_collections) and isset($categories)): ?>
<script>
$(document).ready(function(){
    <?php foreach($all_collections as $value): ?>
        $("#coll_<?php echo $value->slug; ?>").hover(function() {
            var width_window = $( window ).width();
            if (width_window > 767) {
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "110px" );
                }
                $( "#i_n_<?php echo $value->slug; ?>" ).hide();
                $( "#d_<?php echo $value->slug; ?>" ).show();
                $( "#c_<?php echo $value->slug; ?>" ).addClass( "actual-rings-cate-des" );
            }
        },function() {
            var width_window = $( window ).width();
            if (width_window > 767){
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "150px" );
                }
                $( "#c_<?php echo $value->slug; ?>" ).removeClass( "actual-rings-cate-des" );
                $( "#i_n_<?php echo $value->slug; ?>" ).show();
                $( "#d_<?php echo $value->slug; ?>" ).hide();
            }
        });
    <?php endforeach; ?>
    
    <?php foreach($categories as $value): ?>
        $("#cate_<?php echo $value->slug; ?>").hover(function() {
            var width_window = $( window ).width();
            if (width_window > 767) {
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "110px" );
                }
                //$( "#i_n_<?php echo $value->slug; ?>" ).hide();
                //$( "#d_<?php echo $value->slug; ?>" ).show();
                $( "#c_<?php echo $value->slug; ?>" ).addClass( "actual-rings-cate-des" );
            }
        },function() {
            var width_window = $( window ).width();
            if (width_window > 767){
                if (width_window > 991){
                    $( "#container_category" ).find( ".text-description-cate" ).css( "height", "150px" );
                }
                $( "#c_<?php echo $value->slug; ?>" ).removeClass( "actual-rings-cate-des" );
                //$( "#i_n_<?php echo $value->slug; ?>" ).show();
                //$( "#d_<?php echo $value->slug; ?>" ).hide();
            }
        });
    <?php endforeach; ?>
    
});
</script>
<?php endif; ?>
<script src="<?php echo get_option('path_template'); ?>js/script.difusa.js"></script>
</body>
</html>