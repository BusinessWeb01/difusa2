<?php get_header(); ?>
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />
<?php $this->load->view('mensajes_view');?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0 text-left">
			<p>
				<strong>Somos una tienda especializada en vender insumos para cerveza artesanal en México</strong>
			</p>
			<p>
				 DIFUSA es una compañía privada fundada hace más de 40 años. Nuestros clientes son la parte más importante de nuestro negocio, trabajamos duro para cumplir sus requisitos. Nuestros proveedores son compañías globales y los mejores en su clase. Suministramos ingredientes para las industrias de la cerveza y de alimentos.
			</p>
			<br/><br/>
		</div>
	</div>
	<div class="row ">
		<div class="col-md-6 text-left">
			<img src="<?php echo base_url(); ?>content/themes/difusa/img/vision.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-6" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >Visión</h2>
			<p class="text-aboutus-1">
				Convertirse en un proveedor líder de productos de calidad en la industria cervecera, farmacéutica, alimenticia y química, y llegar a ser un socio estratégico de una red ganadora de clientes y proveedores.
			</p>
        </div>
    </div>
	<div class="row ">
        <div class="col-md-6" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >Misión</h2>
			<p class="text-aboutus-1;">
				Controlar y garantizar el suministro de productos, equipos y piezas de calidad, brindando nuestro servicio con prontitud, la realización efectiva de cada etapa de nuestros procesos internos y de comprometerse a la mejora continúa para lograr la satisfacción del cliente.
			</p>
        </div>
		<div class="col-md-6 text-right">
			<img src="<?php echo base_url(); ?>content/themes/difusa/img/mision.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>	
    </div>
	<div class="row ">
		<div class="col-md-3">
			<img src="<?php echo base_url(); ?>content/themes/difusa/img/nuevos.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-9" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>nuevas-adquisiciones" class="link-to-cart">
					Los nuevos productos
				</a>
			</h2>
			<p class="text-aboutus-1">
				Aqui podras visualizar los productos agregados recientemente a nuestro inventario de venta
			</p>
        </div>
    </div>
	<div class="row ">
		<div class="col-md-3">
			<img src="<?php echo base_url(); ?>content/themes/difusa/img/vendidos.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-9" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >
				<!--<a href="<?php echo base_url(); ?>search-products/mas-vendidos" class="link-to-cart">-->
				<a href="<?php echo base_url();?>top_sales_controller/getTopSales">
					Los más vendidos
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los productos que han registrado un mayor numero de compras
			</p>
        </div>
    </div>
	<div class="row ">
		<div class="col-md-3">
			<img src="<?php echo base_url(); ?>content/themes/difusa/img/guia.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-9" style="padding: 8px 8px 8px 8px;  height : 291px;">
           
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>"Complementos class="link-to-cart">
					Guía de productos
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los nuevos productos que están en stock que acaban de agregarse al inventario de productos que se están ofreciendo a los clientes via online.
			</p>
        </div>
    </div>
</div>		
	
<br />
<?php get_footer(); ?>