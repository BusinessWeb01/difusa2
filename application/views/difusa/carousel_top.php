<?php if(isset($carousel_images)): ?>
<div id="<?php echo $carousel_id; ?>" class="carousel slide margen-top-carousel-principal" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php $contador = 0; foreach($carousel_images as $value): ?>
        <div class="item <?php if($contador == 0){ echo 'active'; } ?>">
            <?php if(isset($value['link'])): ?>
                <a href="<?php echo $value['link']; ?>" class="link-img-carousel">
                    <img src="<?php echo $value['path']; ?>" alt="<?php echo 'baner '.$contador; ?>" class="ancho-img-carousel-principal">
                </a>
            <?php else: ?>
                <img src="<?php echo $value['path']; ?>" alt="<?php echo 'baner '.$contador; ?>" class="ancho-img-carousel-principal">
            <?php endif; ?>
        </div>
        <?php $contador++; endforeach; ?>
    </div>

    <?php if($contador > 1): ?>
    <!-- Controls -->
    <a class="left carousel-control control-left-caro-top" href="#<?php echo $carousel_id; ?>" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left caro-flecha-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control control-right-caro-top" href="#<?php echo $carousel_id; ?>" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right caro-flecha-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <?php endif; ?>
</div>
<?php endif; ?>