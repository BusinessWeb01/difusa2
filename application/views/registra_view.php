<?php get_header();?>
<br /><br />
<div class="container animated zoomIn" id="contenedor">
	<div class="form-group">
		<form id="form_register" name="form_register" method="post" action="<?php echo base_url(); ?>registra" onsubmit="return verificar();" autocomplete="off">
		<?php if(isset($error)): ?>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <script type="text/javascript"> 
            		sweetAlert("Oops...","Las contraseñas no coinciden","error");
            	</script>
            </div>
        </div>
        <?php endif; ?> 
        <?php if(isset($error2)):?>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <script type="text/javascript"> 
            		sweetAlert("Oops...","Correo ya registrado","error");
            	</script>
            </div>
        </div>
        <?php endif; ?> 
        <div class="col-xs-4" id="cliente">
        <legend>Datos del cliente:</legend>
			<div class="row">			
				<label>Nombre: </label>
				<input type="text" value ="<?php echo $this->input->post('client_name',true)?>" class="form-control control-contact" id='name' onkeypress="return val(event);" maxlength="80" name="client_name" placeholder="Nombre" required>
			</div>
			<div class="row">
				<label>Apellido Paterno: </label>
				<input type="text" value ="<?php echo $this->input->post('last_name_client',true)?>" class="form-control control-contact"  onkeypress="return val(event);" maxlength="100" name="last_name_client" placeholder="Apellido Paterno" required>
			</div>
			<div class="row">
				<label>Apellido Materno: </label>
				<input type="text" value ="<?php echo $this->input->post('last_name2_client',true)?>" class="form-control control-contact" onkeypress="return val(event);" maxlength="100" name="last_name2_client" placeholder="Apellido Materno"><br>
			</div>			
			<legend>Domicilio </legend>
			<div class="row">
				<label>Estado: </label>
				<input type="text" value ="<?php echo $this->input->post('country',true)?>" class="form-control control-contact" name="country" id="country" maxlength="25" placeholder="Estado" required>
				<!--<select name='country' id='country' class= 'form-control control-contact'>
					<option value='0'>Selecciona tu Estado</option>
					<?php echo $estados?>
				</select>-->
				<label id="municipal">Municipio: </label>
				<input type="text" value ="<?php echo $this->input->post('delegation',true)?>" class="form-control control-contact" name="delegation" id="delegation" maxlength="25" placeholder="Municipio" required>
				<!--<select name="delegation" id="delegation" class="form-control control-contact">
					<option value="0">Selecciona tu Municipio</option>
				</select>-->
				<label>Localidad: </label>
				<input type="text" value ="<?php echo $this->input->post('suburb',true)?>" class="form-control control-contact" name="suburb" id="suburb" maxlength="40" placeholder="Localidad" required>
				<!--<select name="suburb" id="suburb" class="form-control control-contact">
					<option value="0">Selecciona tu Localidad</option>
				</select>-->
				<label>Calle: </label>
				<input type="text" value ="<?php echo $this->input->post('street',true)?>" class="form-control control-contact" name="street" id="street" maxlength="25" placeholder="Calle" required>
				<label>N° exterior: </label>
				<input type="text" value ="<?php echo $this->input->post('outdoor',true)?>" class="form-control control-contact" name="outdoor" id="outdoor" maxlength="18" placeholder="N° exterior" required>
				<label>N° interior: </label>
				<input type="text" value ="<?php echo $this->input->post('indoor',true)?>" class="form-control control-contact" name="indoor" id="indoor" maxlength="18" placeholder="N° interior">
				<label>C&oacute;digo Postal: </label>
				<input type="text" value ="<?php echo $this->input->post('zipcode',true)?>" class="form-control control-contact" name="zipcode" id="zipcode" onkeypress="return Numeros(event);" maxlength="5" placeholder="C.P."required>
				<br>
			</div>
			<legend>Contacto</legend>
			<div class="row">
				<label>N&uacute;mero de tel&eacute;fono: </label>
				<input type="text" value ="<?php echo $this->input->post('local',true)?>" class="form-control control-contact" onkeypress="return vale(event);" id="local" name="local" maxlength="16" placeholder="(Clave Lada) xx-xx-xx-xx" required>
				<label>N&uacute;mero de celular: </label>
				<input type="text" value ="<?php echo $this->input->post('celular',true)?>" class="form-control control-contact" id="celular" name="celular" onkeypress="return vale(event);" maxlength="15" placeholder="Celular" ><br>
			</div>
			<legend>Datos de sesi&oacute;n </legend>
			<h6>*Se enviara un mensaje al correo que proporciones</h6>
			<div class="row">
				<label>Correo: (Tu usuario para iniciar sesi&oacute;n)</label>
				<input type="email" value ="<?php echo $this->input->post('usuario',true)?>" class="form-control control-contact" id="usuario" maxlength="70" name="usuario" placeholder="Usuario" required>
			</div>			
			<div class="row">
				<label>Contraseña: </label>
				<input type="password" class="form-control control-contact" id="password" minlength="8" maxlength="20" name="password" placeholder="Contraseña" required>
			</div>
			<div class="row">
				<label>Verificar contraseña: </label>
				<input type="password" class="form-control control-contact" id="password2" minlength="8" maxlength="20" name="password2" required><br>
			</div>			
			<br>
		</div>			
		<div class="col-xs-4" id="factura">
			<legend>Domicilio de facturación</legend>
			<div class="row">
				<input type="checkbox" name="copy" id="copiar" onchange="copyinfo(this);">&nbsp;El domicilio de facturaci&oacute;n es el mismo que el de envio.
				<label>Estado: </label>
				<input type="text" value ="<?php echo $this->input->post('country2',true)?>" class="form-control control-contact" name="country2" id="country2" maxlength="25" placeholder="Estado" required>
				<!--<select name='country2' id='country2' class= 'form-control control-contact' required>
					<option value='0'>Selecciona tu Estado</option>
				<?php echo $estados;?>
				</select>-->
				<label id="municipal2">Municipio: </label>
				<input type="text" value ="<?php echo $this->input->post('delegation2',true)?>" class="form-control control-contact" name="delegation2" id="delegation2" maxlength="25" placeholder="Estado" required>
				<!--<select name="delegation2" id="delegation2" class="form-control control-contact" required>
					<option value="0">Selecciona tu Municipio</option>
				</select>-->
				<label>Localidad: </label>
				<input type="text" value ="<?php echo $this->input->post('suburb2',true)?>" class="form-control control-contact" name="suburb2" id="suburb2" maxlength="40" placeholder="Estado" required>
				<!--<select name="suburb2" id="suburb2" class="form-control control-contact" required>
					<option value="0">Selecciona tu Localidad</option>
				</select>-->
				<label>Calle: </label>
				<input type="text" value ="<?php echo $this->input->post('street2',true)?>" class="form-control control-contact" name="street2" id="street2" maxlength="25" placeholder="Calle" required>
				<label>N° exterior: </label>
				<input type="text" value ="<?php echo $this->input->post('outdoor2',true)?>" class="form-control control-contact" name="outdoor2" id="outdoor2" maxlength="10" placeholder="N° exterior" required>
				<label>N° interior: </label>
				<input type="text" value ="<?php echo $this->input->post('indoor2',true)?>" class="form-control control-contact" name="indoor2" id="indoor2" maxlength="10" placeholder="N° interior" >
				<label>C&oacute;digo Postal: </label>
				<input type="text" value ="<?php echo $this->input->post('zipcode2',true)?>" class="form-control control-contact" name="zipcode2" id="zipcode2" maxlength="5" placeholder="C.P." required>
				<label>RFC: </label>
				<input type="text" value ="<?php echo $this->input->post('rfc',true)?>" class="form-control control-contact" name="rfc" id="rfc" maxlength="13" placeholder="RFC" required>
				<br>
			</div>
			<legend>Contacto </legend><br>
			<div class="row">
				<label>N&uacute;mero de tel&eacute;fono: </label>
				<input type="text" value ="<?php echo $this->input->post('local2',true)?>" class="form-control control-contact" id="local2" name="local2" onkeypress="return vale(event);" maxlength="12" placeholder="(Clave Lada) xx-xx-xx-xx" required>
				<label>N&uacute;mero de celular: </label>
				<input type="text" value ="<?php echo $this->input->post('celular2',true)?>" class="form-control control-contact" onkeypress="return vale(event);" id="celular2" name="celular2" maxlength="15" placeholder="Celular" required>
				<label>Correo Electronico: </label>
				<input type="email" value ="<?php echo $this->input->post('email2',true)?>" class="form-control control-contact" id="email2" name="email2" maxlength="55" placeholder="E-mail" required><br>
			</div>
			<legend>Suscripciones: </legend>
			<div class="row">
				<input type="checkbox" name="suscribir" onchange="muestraSuscripcion(this);" id="suscribeme">&nbsp;Suscribirse a algun boletin de noticias
			</div><br>
			<div id="suscribe">
			<?php
				for($i=0;$i<count($news);$i++)
				{
					?>
					<div class="col-ms-4 col-xs-offset-0">
						<div class="form-group">
					<?php
					echo "<input type='checkbox' name='newsuser[]' value ='".$news[$i]['id_subscription']."'>&nbsp;".$news[$i]['subscription_name']."";
					?>
						</div>
					</div>
					<?php
				}
			?>
			</div><br><br>
			<div class="row">
				<b><input type="checkbox" id="acepto">&nbsp;Acepto el <a href="<?php echo base_url();?>documento/aviso-de-privacidad" target="_blank">aviso de privacidad</a> y <a target="_blank" href="<?php echo base_url()?>documento/politicas-de-envio">politicas de envio</a></b>
			</div>
		</div>
		<legend></legend>
			<center><button type="submit" name="enviar" id="enviar" class="btn btn-danger">Registrate ya!</button></center>
		</form>
	</div>
</div><br>
<?php get_footer(); ?>