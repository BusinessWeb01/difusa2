<?php if($this->session->userdata('item')): ?>
    <div class="row">
    	<div class="colmd-12 col-md-offset-0">
    		<script type="text/javascript"> 
    			sweetAlert("Bienvenido","Sesión iniciada correctamente","success");
    		</script>
    	</div>
    </div><?php $this->session->unset_userdata('item');?>
<?php endif;?>
<?php if($this->session->userdata('registrook')): ?>
	<div class="row">
		<div class="colmd-12 col-md-offset-0">
			<script type="text/javascript"> 
            	sweetAlert("Felicidades","Bienvenido al sistema de compra de DIFUSA.\nPor favor revisa tu correo para activar tu cuenta","success");
            </script>
		</div>
	</div><?php $this->session->unset_userdata('registrook');?>
<?php endif;?>
<?php if($this->session->userdata('activado')): ?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
			<script type="text/javascript"> 
            	sweetAlert("Felicidades","Tu cuenta ha sido activada","success");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('activado');?>
<?php endif;?>
<?php if($this->session->userdata('activaerror')): ?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
			<script type="text/javascript"> 
            	sweetAlert("Oops...","Error al activar tu cuenta\nEnvia un correo a contacto@difusa.com.mx notificando el error","error");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('activaerror');?>
<?php endif;?>
<?php if($this->session->userdata('logout')):?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
                swal("Hasta luego","Sesión cerrada exitosamente.","info");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('logout');?>
<?php endif;?>
<?php if($this->session->userdata('recuperar')):?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
                swal("","Mensaje enviado a tu correo, por favor revisa tu bandeja de entrada","info");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('recuperar');?>
<?php endif;?>
<?php if($this->session->userdata('Recuperado')):?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
                swal("Exito","Contraseña cambiada exitosamente","success");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('Recuperado');?>
<?php endif;?>
<?php if($this->session->userdata('Recuperadoerror')):?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
                swal("Oops..","Error al cambiar la contraseña, intentalo de nuevo","error");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('Recuperadoerror');?>
<?php endif;?>
<?php if($this->session->userdata('admin')): ?>
    <div class="row">
        <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
                sweetAlert("Bienvenido","Sesión iniciada correctamente como administrador","success");
            </script>
        </div>
    </div><?php $this->session->unset_userdata('admin');?>
<?php endif;?>