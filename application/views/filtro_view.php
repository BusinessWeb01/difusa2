<?php
  if(isset($nombre2))
  {
    if(strcmp($nombre2,'%%') === 0) $nombre2 = '';
    $nombre2 = str_replace('%', '', $nombre2);
    $inputNombre = "<input type='text' name='nombreProducto' class='filtro' placeholder='Nombre del producto...' id='nombre' value='$nombre2'>";
  }
  else
    $inputNombre = "<input type='text' name='nombreProducto' class='filtro' placeholder='Nombre del producto...' id='nombre'>";
?>
<?php if($this->session->userdata('filtroempty')):?>
    <div class="row">
      <div class="colmd-12 col-md-offset-0">
        <script type="text/javascript"> 
          sweetAlert("Oops..","Debe al menos seleccionar una opcion del filtro o colocar mas de 3 caracteres en el nombre del producto","error");
        </script>
      </div>
    </div><?php $this->session->unset_userdata('filtroempty');?>
<?php endif;?>
<div class="container">
<?php //echo $seleccionMarca." ".$seleccionPais." ".$seleccionPresentacion;?>
  <form method="post" action="<?php echo base_url();?>filtro/<?=$subcategoria?>/1" name="filter">
    Nombre:
    <?=$inputNombre;?>
    Marca:
      <select name="marca" class="filtro" id="marca">
        <option value='0'>Selecciona una opción</option>
        <?php
          for($c=0;$c<count($marca);$c++)
          {
            if(strcmp($seleccionMarca,$marca[$c]['id_brand']) === 0)
              echo "<option value='".$marca[$c]['id_brand']."' selected>".$marca[$c]['brand']."</option>";
            else
              echo "<option value='".$marca[$c]['id_brand']."'>".$marca[$c]['brand']."</option>";
          }
        ?>
      </select>
      Pais:
      <select name="pais" class="filtro" id="pais">
        <option value='0'>Selecciona una opción</option>
        <?php
          for($d=0;$d<count($pais);$d++)
          {
            if(strcmp($seleccionPais,$pais[$d]['country_name_iso3']) === 0)
              echo "<option value='".$pais[$d]['country_name_iso3']."' selected>".$pais[$d]['country_name']."</option>";
            else
              echo "<option value='".$pais[$d]['country_name_iso3']."'>".$pais[$d]['country_name']."</option>";
          }
        ?>
      </select>
      Presentación:
      <select name="presentacion" class="filtro" id="presentacion">
        <option value='0'>Selecciona una opción</option>
        <?php
          for($c=0;$c<count($presentacion);$c++)
          {
            if(strcmp($seleccionPresentacion,$presentacion[$c]['id_presentation']) === 0)
              echo "<option value='".$presentacion[$c]['id_presentation']."' selected>".$presentacion[$c]['presentation']."</option>";
            else
              echo "<option value='".$presentacion[$c]['id_presentation']."'>".$presentacion[$c]['presentation']."</option>";
          }
        ?>
      </select>
        <button type="submit" id="filtro" class="btn btn-danger">Filtrar</button>
  </form>
</div>
<br><br>