<?php get_header();?><br><br>
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"><?php echo $mostrar['title'];?></h1>
        </div>
    </div>
</div>
<div class="container">
<br>
<?=$mostrar['content'];?>
</div>
<br>
<?php get_footer();?>