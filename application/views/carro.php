<?php get_header(); ?><br><br>
<div class="container">
    <div class="row">
    <?php $this->load->view('carro_mensajes_view');?>
        <div class="col-md-12 col-md-offset-0">
            <?php
            //si el carrito contiene productos los mostramos
            if($carrito) 
            {
                $total=0;
                ?>
                <div class="grid_5" id="contenidoCarrito">
                    <table class="carro">
                    <?php
                        if(isset($cancel))
                        {
                    ?><center>
                        <div class="col-md-12 col-md-offset-0" style="text-align:center;">
                            <p class="bg-danger">Compra Cancelada</p>
                    </div></center>
                    <?php
                        }
                        ?>
                       <legend>Carrito de la compra</legend>
                        <tr>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                            <th></th>
                        </tr>
                    <?php
                    foreach ($carrito as $item)
                    {
                        $precio = $item['price'];
                        $divisaActual = $item['currency'];
                        $divisaDestino = $this->session->userdata('destino_cambio');
                        if((strcmp($divisaActual,$divisaDestino)) !== 0)
                        {
                            if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'EUR') === 0)
                                $precio *= $this->session->userdata('pesoAEuro');
                            if(strcmp($divisaActual,'MXN') === 0 && strcmp($divisaDestino,'USD') === 0)
                                $precio *= $this->session->userdata('pesoADolar');
                            if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'MXN') === 0)
                                    $precio *= $this->session->userdata('euroAPeso');
                            if(strcmp($divisaActual,'EUR') === 0 && strcmp($divisaDestino,'USD') === 0)
                                $precio *= $this->session->userdata('euroADolar');
                            if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'MXN') === 0)
                                $precio *= $this->session->userdata('dolarAPeso');
                            if(strcmp($divisaActual,'USD') === 0 && strcmp($divisaDestino,'EUR') === 0)
                                $precio *= $this->session->userdata('dolarAEuro');
                        }
                        ?>
                        <tr>
                            <td><?= ucfirst($item['name']) ?></td>
                            <td>
                                <?php
                                $nombres = array('nombre' => ucfirst($item['name']));
                                if($this->cart->has_options($item['rowid']))
                                {
                                    foreach ($this->cart->product_options($item['rowid']) as $opcion => $value) 
                                    {
                                        echo " <em>".$value."</em>";
                                    }
                                }
                                ?>
                            </td>
                            <td><?php echo number_format($precio,2,'.',',')." ".$this->session->userdata('destino_cambio');
                                    $precio = round($precio,2);
                                    $precio *= $item['qty'];
                                    $total += $precio;
                            ?></td>
                            <td><?= $item['qty'] ?></td>
                            <!--creamos el enlace para eliminar el producto
                            pulsado pasando el rowid del producto-->
                            <td><?php echo number_format($precio,2,'.',',')." ".$this->session->userdata('destino_cambio');?></td>
                            <td id="eliminar"><?= anchor('cart_controller/eliminarProducto/' .$item['rowid'], 'Eliminar') ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="total">
                        <td></td><td><strong>Total:</strong></td>
                        <td></td>
                        <td><?php echo $this->cart->total_items();?></td>
                        <?php 
                            if($this->cart->total() >= 5000 && $this->cart->total() < 20000)
                            {
                                $total *= 0.925;
                            }
                            if($this->cart->total() >= 20000)
                            {
                                $total *= 0.85;
                            }
                        ?>
                        <td><?= number_format($total,2,'.',',')." ".$this->session->userdata('destino_cambio');?> </td>
                        <td>
        <form method="post" action="<?php echo base_url()?>manda-carro">
            <input type="image" name="submit" alt="Haga pagos con PayPal: es rápido, sin costo y seguro" src="http://www.paypal.com/es_XC/i/btn/x-click-but01.gif" border="0">
</form>
                        </td>
                    </tr>
                </table>
                <?= anchor('cart_controller/eliminarCarrito/', 'Vaciar carrito')?>
                </div>
            <?php
            }
            else
            {
            	echo '<h3>No hay artículos en el carrito.</h3>';
            }
            ?>
        </div>
    </div>
</div>
<br /><br />
<div style="margin-top: 300px;"></div>
<?php get_footer(); ?>