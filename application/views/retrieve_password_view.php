<?php get_header();?>
<br /><br />
<div class="container animated zoomIn" id="contenedor">
<legend><h2>Recupera tu contraseña</h2></legend>
<p>Por favor indica tu dirección de correo electronico al cual te mandaremos un mensaje de recuperación de contraseña</p>
<div class="col-xs-4">
	<div class="form-group">
		<form name="form_register" onsubmit="transicion();" method="post" action="<?php echo base_url();?>remember-password" autocomplete="off"> 
		<?php if(isset($error)): ?>
        <div class="row">
            <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
            sweetAlert("Oops...","Correo inexistente","error");
            </script>
            </div>
        </div>
        <?php endif;?>
        <?php if(isset($desactivado)):?>
        <div class="row">
            <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
            sweetAlert("Oops...","Parece que tu cuenta esta inactiva\nPor favor revisa la bandeja de entrada de tu correo para activar tu cuenta","error");
            </script>
            </div>
        </div>
        <?php endif;?>
			<div class="row">
				<label>Correo: </label>
				<input type="text" class="form-control control-contact" id="name" name="mail" placeholder="Correo" autocomplete="off">
			</div>	
			<br><br><div class="colmd-12 col-md-offset-0"><button type="submit" name="enviar" id="enviar" class="btn btn-danger" onclick="">Reestablecer contraseña</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		</form>
	</div>
</div>
</div><br><br><br><br><br><br><br><br><br><br><br><br>
<?php get_footer();?>