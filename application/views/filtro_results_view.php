<?php get_header();?>
<br><br>
<?php $this->load->view('filtro_view');?>
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">Resultados</h1>
        </div>
    </div>
</div>
<br><br>

<div id="productos">
<?php
if(count($resultados)<1)
	echo "<br><br><h2>No se han encontrado resultados</h2><br><br><br><br><br><br>";
else
{
	for($i=0;$i<count($resultados);$i++) 
	{
		if(!$this->session->userdata('id_client'))
		{
		?>
		<form id="form1" name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>info-producto/<?php echo $resultados[$i]['id_product'];?>" style="display: inline-block;">
			<a class="linkcart" href="<?php echo base_url();?>info-producto/<?php echo $resultados[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
				<div id="cajita<?php echo $i;?>" class="boxpr2 animated bounceIn">
					<img class="imagpr" src="<?php echo base_url();echo $resultados[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
					<br>
					<br>
					Nombre:
  					<?php
	  					echo $resultados[$i]['product_name'];
  					?> 
					<br>
					Código:
   					<?php
	  					echo $resultados[$i]['product_code'];
  					?> 
					<br>Marca:
   					<?php
	  					echo $resultados[$i]['brand'];
  					?> 
					<br>Precio:
   					<?php
						echo number_format($resultados[$i]['price'], 2, '.', ',');
  					?> 
   					<?php
	  					echo $resultados[$i]['currency_name'];
  					?>
  					<br>Presentación:
   					<?php
	  					echo $resultados[$i]['presentation'];
  					?> 
				</div>
 			</a>
		</form>
  		<?php
  		}
  		else
  		{
  			if(strcmp($resultados[$i]['id_category'],'INS') === 0)
  				$inicioForm = "<form id='form1' name='form$i' method='post' action='".base_url()."info-insumo/$resultados[$i]['id_product']' style='display: inline-block;'>";
  			else
  				$inicioForm = "<form id='form1' name='form$i' method='post' action='".base_url()."info-complemento/$resultados[$i]['id_product']' style='display: inline-block;'>";
  		?>
		<?=$inicioForm?>
			<a class="linkcart" href="<?php echo base_url();?>info-producto/<?php echo $resultados[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
				<div id="cajita<?php echo $i;?>" class="boxpr2 animated bounceIn">
					<img class="imagpr" src="<?php echo base_url();echo $resultados[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
					<br>
					<br>
					Nombre:
  					<?php
	  					echo $resultados[$i]['product_name'];
  					?> 
					<br>
					Código:
   					<?php
	  					echo $resultados[$i]['product_code'];
  					?> 
					<br>Marca:
   					<?php
	  					echo $resultados[$i]['brand'];
  					?> 
					<br>Precio:
   					<?php
						echo number_format($resultados[$i]['price'], 2, '.', ',');
  					?> 
   					<?php
	  					echo $resultados[$i]['currency_name'];
  					?>
  					<br>Presentación:
   					<?php
	  					echo $resultados[$i]['presentation'];
  					?> 
				</div>
 			</a>
		</form>
		<?php
  		}
  	}
}
  		?>
</div>
<div id="paginador">
	<?php 
		$totalResultados /= 8;
		$totalResultados = ceil($totalResultados);
		for($c=1;$c<=$totalResultados;$c++)
		{
			if($c == $pagina)
			{
			?>
				<div class="activo"><?php echo $c?></div>
			<?php
			}
			else
			{
			?>
				<form method="post" action="<?php echo base_url();?>filtro/<?=$subcategoria?>/<?=$c?>" name="filter<?=$c?>" style="display:inline">
				<div class="pag" onclick="document.forms['filter<?=$c?>'].submit()"><?php echo $c?></div>
				<input type="hidden" name="nombreProducto" value="<?=$nombre2?>"/>
				<input type="hidden" name="marca" value="<?=$seleccionMarca?>"/>
				<input type="hidden" name="pais" value="<?=$seleccionPais?>"/>
				<input type="hidden" name="presentacion" value="<?=$seleccionPresentacion?>"/>
				</form>
			<?php 
			}
			?>
	<?php 
		}
	?>
</div>
<br><br>
<?php get_footer();?>