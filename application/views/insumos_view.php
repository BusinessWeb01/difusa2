<?php get_header();?>
<br><br>

<?php $this->load->view('filtro_view');?>
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">
         <?php
         	echo $nombre['sub_category'];
         ?>
         </h1>
        </div>
    </div>
</div>
<br><br>

<div id="productos">
<?php
	for($i=0;$i<count($info);$i++) 
	{
?>
		<form id="form1" name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>info-producto/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">
			<a class="linkcart" href="<?php echo base_url();?>info-producto/<?php echo $info[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
				<div id="cajita<?php echo $i;?>" class="boxpr2 animated bounceIn">
					<img class="imagpr" src="<?php echo base_url();echo $info[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
					<br>
					<br>
					Nombre:
  					<?php
	  					echo $info[$i]['product_name'];
  					?> 
					<br>
					Código:
   					<?php
	  					echo $info[$i]['product_code'];
  					?> 
					<br>Marca:
   					<?php
	  					echo $info[$i]['brand'];
  					?> 
					<br>Precio:
   					<?php
						echo number_format($info[$i]['price'], 2, '.', ',');
  					?> 
   					<?php
	  					echo $info[$i]['currency_name'];
  					?>
  					<br>Presentación:
   					<?php
	  					echo $info[$i]['presentation'];
  					?> 
					<br><p class="hidde">Precio descuento:</p>
 					<?php
  						echo $info[$i]['before_price'];
	  				?> 
				</div>
 			</a>
		</form>
  		<?php
  	}
  		?>
</div>
<div id="paginador">
	<?php 
		$total /= 8;
		$total = ceil($total);
		for($c=1;$c<=$total;$c++)
		{
			if($c == $pagina)
			{
			?>
				<div class="activo"><?php echo $c?></div>
			<?php
			}
			else
			{
			?>
				<a href="<?php echo base_url();?>Insumos/<?=$subcategoria?>/<?=$c?>"><div class="pag"><?php echo $c?></div></a>
			<?php 
			}
			?>
	<?php 
		}
	?>
</div>
<br><br>
<?php get_footer();?>