<?php get_header(); ?>
<br /><br />
<div class="container animated zoomIn" id="contenedor">
<div class="col-xs-4">
<legend><h2>Inicio de sesión</h2></legend>
	<div class="form-group">
		<form name="form_register" onsubmit="transicion(); return validaLogin();" method="post" action="<?php echo base_url();?>validate/" autocomplete="off"> 
		<?php if(isset($error)):?>
        <div class="row">
            <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
            sweetAlert("Oops...","Usuario o contraseña incorrectos","error");
            </script>
            </div>
        </div>
        <?php endif;?>
        <?php if(isset($desactivado)): ?>
        <div class="row">
            <div class="colmd-12 col-md-offset-0">
            <script type="text/javascript"> 
            sweetAlert("Oops...","Parece que tu cuenta esta inactiva\nPor favor revisa la bandeja de entrada de tu correo para activar tu cuenta","error");
            </script>
            </div>
        </div>
        <?php endif;?>
			<div class="row">
				<label>Usuario: </label>
				<input type="text" class="form-control control-contact" id="name" name="username" placeholder="Usuario" autocomplete="off">
			</div><br>
			<div class="row">
				<label>	Contraseña: </label>
				<input type="password" class="form-control control-contact" id="password" name="password" placeholder="Contraseña">
			</div>
			<br><div class="colmd-12 col-md-offset-0"><button type="submit" name="enviar" id="enviar" class="btn btn-danger" onclick="">Iniciar Sesi&oacute;n</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!--<a href="<?php echo base_url();?>remember-passwords" target="_blank">Has olvidado tu contraseña?</a>--></div>
		</form>
	</div>
</div>
</div><br><br><br><br><br><br><br><br>
<?php get_footer();?>