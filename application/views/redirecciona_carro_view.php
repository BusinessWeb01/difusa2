<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo site_title(); ?></title>
    <script src="<?php echo base_url(); ?>content/themes/difusa/js/jquery-1.11.2.min.js"></script>
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/numeral.min.js"></script>
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/jquery.elevateZoom-3.0.8.min.js"></script>
        <link href="<?php echo base_url(); ?>content/themes/difusa/css/style.difusa.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style1.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/script.difusa2.js"></script>
        <script src="<?php echo base_url(); ?>content/themes/difusa/js/sweetalert.min.js"></script>
        
</head>
<body>
<form name="formTpv" method="post" action="https://www.paypal.com/cgi-bin/webscr">
    <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="business" value="a.administracion@difusa.com.mx">
<?php   
        if($carrito)
        {
            $i = 1;
            foreach($carrito as $item)
            {
                $precio = $item['price'];
                $divisaActual = $item['currency'];
                $divisaDestino = $this->session->userdata('destino_cambio');
                if((strcmp($divisaActual,$divisaDestino)) !== 0)
                {
                    $precio = $this->lb_moneda->conversor_monedas($divisaActual,$divisaDestino,$precio);
                }
            ?>
                <input type="hidden" name="item_name_<?=$i?>" value="<?php echo ucfirst($item['name']);?>" />
                <input type="hidden" name="quantity_<?=$i?>" value="<?php echo $item['qty']; ?>">
                <input type="hidden" name="item_number_<?=$i?>" value="<?php echo $this->cart->product_options($item['rowid'])['code'];?>" />
                <input type="hidden" name="amount_<?=$i?>" value="<?php echo number_format($precio, 2, '.', ','); ?>" />
            <?php
            $i++;
            }
        }
        ?>
        <?php
            if($this->cart->total() >= 5000 && $this->cart->total() < 20000)
                echo '<input type="hidden" name="discount_rate_cart" value=".7.5">';
            if($this->cart->total() >= 20000)
                echo '<input type="hidden" name="discount_rate_cart" value="15">';
        ?>
<input type="hidden" name="page_style" value="primary">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="return" value="<?= base_url()?>cart_controller/success">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="cancel_return" value="<?= base_url()?>cart_controller/cancel">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="currency_code" value="<?= $this->session->userdata('destino_cambio');?>">
<input type="hidden" name="cn" value="PP-BuyNowBF">
<input type="hidden" name="first_name" value="<?php echo $this->session->userdata('client_name');?>">
<input type="hidden" name="last_name" value="<?php echo $this->session->userdata('last_name_client');?>">
<input type="hidden" name="address1" value="<?php echo $this->session->userdata('street_bussiones').' No. Ext. '.$this->session->userdata('outdoor_number').' No. In. '.$this->session->userdata('indoor_number');?>">
<input type="hidden" name="address2" value="<?php echo $this->session->userdata('suburb_bussiones');?>">
<input type="hidden" name="city" value="<?php echo $this->session->userdata('city_bussiness');?>">
<input type="hidden" name="zip" value="<?php echo $this->session->userdata('zipcode_bussiness');?>">
<input type="hidden" name="night_phone_a" value="<?php echo $this->session->userdata('phone_number_bussiness');?>">
<input type="hidden" name="night_phone_b" value="<?php echo $this->session->userdata('cellphone_number_bussiness');?>">
<input type="hidden" name="lc" value="es">
<input type="hidden" name="country" value="">
</form>
</body></html>