<?php if($this->session->userdata('descuento1')): ?>
    <div class="row">
    	<div class="colmd-12 col-md-offset-0">
    		<script type="text/javascript"> 
    			sweetAlert("Felicidades!!!","Descuento del 7.5% aplicado al total de su compra","success");
    		</script>
    	</div>
    </div><?php $this->session->unset_userdata('item');?>
<?php endif;?>
<?php if($this->session->userdata('descuento2')): ?>
    <div class="row">
    	<div class="colmd-12 col-md-offset-0">
    		<script type="text/javascript"> 
    			sweetAlert("Felicidades!!!","Descuento del 15% obtenido aplicado al total de su compra","success");
    		</script>
    	</div>
    </div><?php $this->session->unset_userdata('item');?>
<?php endif;?>
<?php if($this->session->userdata('compra')): ?>
    <div class="row">
    	<div class="colmd-12 col-md-offset-0">
    		<script type="text/javascript"> 
    			swal({
    				title: "Carrito vacio",
    				text:  "Parece que aun no tienes articulos en tu carrito\nDale un vistazo a nuestras secciones de insumos y complementos,  estamos seguros que algun articulo te gustara",
    				imageUrl: "img/img001.jpg"});
    		</script>
    	</div>
    </div><?php $this->session->unset_userdata('compra');?>
<?php endif;?>