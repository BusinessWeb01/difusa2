<?php get_header();?>
<br />
<br />
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">
         		Resultados de la busqueda por&nbsp;<?php echo $this->session->userdata('opcion');?>
	        </h1>
        </div>
    </div>
</div>
<br><br><br>
<div class="container" style="text-align:center;">
<?php 
if($total > 0)
{
for($i = 0 ; $i < count($info) ; $i++)
{
	if(strcmp($info[$i]['id_category'],'INS') === 0)
	{
	?>
	<form id="form1"  name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>info-insumo/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">
<a class="linkcart" href="<?php echo base_url();?>info-insumo/<?php echo $info[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
<?php }else{?>
	<form id="form1"  name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>info-complemento/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">

<a class="linkcart" href="<?php echo base_url();?>info-complemento/<?php echo $info[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
<?php }?>
	<div class="boxpr2 col-md-4" >
		<img class="imagpr" src="<?php echo base_url();echo $info[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
		<br>
		<br>
		Nombre: 
  		<?php
  			echo $info[$i]['product_name'];
  		?> 
		<br>
		Código:
   		<?php
  			echo $info[$i]['product_code'];
  		?> 
		<br>Marca:
   		<?php
  			echo $info[$i]['brand'];
		?>
		<br>Precio:
   		<?php
  			echo number_format($info[$i]['price'], 2, '.', ',');
  		?> 
   		<?php
  			echo $info[$i]['currency_name'];
  		?> 
  		<br>Presentación:
   		<?php
			echo $info[$i]['presentation'];
  		?>
		<br><p class="hidde">Precio descuento:</p>
 		<?php
  		echo $info[$i]['before_price'];
  		?> 
 </a>
 <a class="linkcart" href="#popup<?php echo $i; ?>">
  <div ><img src="<?php echo base_url();?>img/cart.gif"/><br>
    Agregar al carrito de compras</div>
  </a>
 </form>
 </div>
 <div class="overlay" id="popup<?php echo $i;?>" style="z-index:1;">
  <div class="popup">
    <h3>Agregar al carrito</h3><br>
    <a class="close" href="#">&times;</a>
    <div class="content" style="position:relative;">
    <?php
    if(strcmp($info[$i]['id_category'],'INS') === 0)
	{
	?>
    <form action="<?php echo base_url(); ?>agrega-insumo"  method="post">
    <?php
    }
    else
    {
    ?>
    <form action="<?php echo base_url(); ?>agrega-complemento"  method="post">
    <?php
    }
    ?>
    <input type="hidden" name="idproduct" value="<?php echo  $info[$i]['id_product'];?>">
    <h4 class="text-price-product">
    	Nombre: <span id="the_price_now" class="number-price-product"><?php echo  $info[$i]['product_name']; ?></span>
    </h4>
    <h4 class="text-price-product">
    	Presentación: <span id="the_price_now" class="number-price-product"><?php echo  $info[$i]['presentation']; ?></span>
    </h4>
    <h4 class="text-price-product">
    	Código: <span id="the_price_now" class="number-price-product"><?php echo  $info[$i]['product_code']; ?></span>
    </h4>
    <h4 class="text-price-product">
    	Precio: <span id="the_price_now" class="number-price-product"><?php echo  number_format($info[$i]['price'], 2, '.', ','); ?><?php echo  $info[$i]['currency_name']; ?></span>
	</h4>
    <input type="hidden" name="nombre" value="<?php echo  $info[$i]['product_name']; ?>">
    <input type="hidden" name="codigo" value="<?php echo  $info[$i]['product_code']; ?>">
    <input type="hidden" name="presentacion" value="<?php echo  $info[$i]['presentation']; ?>">
    <input type="hidden" name="precio" value="<?php echo  $info[$i]['price']; ?>">
    <input type="hidden" name="moneda" value="<?php echo  $info[$i]['currency_name']; ?>">
    Cantidad: <br><input type="number" name="cantidad" min="1" value="1" style="width:80px;">
    <br><br><input type="submit" class="botoncart" value="Agregar">
    </form>
    </div>
  </div>
</div>
<?php 
	}
	$total /=9;
?>
</div>
<div style="text-align:center;">
<?php 
	for($c=1;$c<=$total;$c++)
	{
		if($c == $pagina)
		{
		?>
			<div class="activo"><?php echo $c?></div>
		<?php
		}
		else
		{
		?>
			<a href="<?php echo base_url();?>search/<?php echo $c?>"><div class="pag"><?php echo $c?></div></a>
		<?php 
		}
		?>
<?php }
?>
</div>
<br><br><br><br><br><br><br><br>
<?php 
}
else
	{?>
		<h1>No se han encontrado resultados</h1><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<?php 
	}?>
	</div>
<?php get_footer()?>