<?php get_header();?>
<br />
<br />
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">
         		Resultados de la busqueda por&nbsp;<?php echo $this->session->userdata('opcion');?>
	        </h1>
        </div>
    </div>
</div>

<br><br><br>
<div class="container" style="text-align:center;">
<?php 
if($total > 0)
{
for($i = 0 ; $i < count($info) ; $i++)
{
	?>
<form id="form1"  name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>informacion/producto/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">

<a class="linkcart" href="<?php echo base_url();?>info-producto/<?php echo $info[$i]['id_product'];?>#visualizar" onclick="javascript:document.form<?php echo $i;?>.submit();">
	<div class="boxpr2 col-md-4" >
		<img class="imagpr" src="<?php echo base_url();echo $info[$i]['url_images'];?>" alt="Imagen no encontrada" onError="this.onerror=null;this.src='<?php echo base_url().'Catalogo/IND.jpg';?>';"/>
		<br>
		<br>
		Nombre: 
  		<?php
  			echo $info[$i]['product_name'];
  		?> 
		<br>
		Código:
   		<?php
  			echo $info[$i]['product_code'];
  		?> 
		<br>Marca:
   		<?php
  			echo $info[$i]['brand'];
		?>
		<br>Precio:
   		<?php
  			echo number_format($info[$i]['price'], 2, '.', ',');
  		?> 
   		<?php
  			echo $info[$i]['currency_name'];
  		?> 
  		<br>Presentación:
   		<?php
			echo $info[$i]['presentation'];
  		?>
		<br><p class="hidde">Precio descuento:</p>
 		<?php
  		echo $info[$i]['before_price'];
  		?> 
 </a>
 </form>
 </div>
<?php 
	}
	$total /=9;
	
?>
</div>
<div style="text-align:center;">
<?php 
	for($c=1;$c<=$total;$c++)
	{
		if($c == $pagina)
		{
		?>
			<div class="activo"><?php echo $c?></div>
		<?php
		}
		else
		{
		?>
			<a href="<?php echo base_url();?>search/<?php echo $c?>"><div class="pag"><?php echo $c?></div></a>
		<?php 
		}
		?>
<?php }
?>
</div>
<br><br><br><br><br><br><br><br>
<?php 
}
else
	{?>
		<h1>No se han encontrado resultados</h1><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<?php 
	}?>
	</div>
<?php get_footer()?>