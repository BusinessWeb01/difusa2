<?php
$form_attributes = array(
	'class' => 'form-signin',
	'id' => 'login_form',
	'role' => 'form'
);
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'class' => 'form-control',
	'placeholder' => 'Usuario'
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class' => 'form-control',
	'placeholder' => 'Contraseña'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember')
);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>tvr</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
  </head>
  <body>
    <div class="container">

      <?php echo form_open($this->uri->uri_string(), $form_attributes); ?>
        <h2 class="form-signin-heading">Administrador</h2>
		
        <?php echo form_input($login); ?>
        <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
		
		<?php echo form_password($password); ?>
		<?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
        
		<label class="checkbox">
		  <?php echo form_checkbox($remember); ?> Recordarme
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      <?php echo form_close(); ?>
    </div>
  </body>
</html>