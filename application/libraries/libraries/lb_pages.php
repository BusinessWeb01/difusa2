<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_pages
{
    public function get_page($url = ''){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $page = $CI->md_pages->get_page_by('slug', $url);
        return $page;
    }
}