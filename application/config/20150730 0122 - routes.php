<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages";
$route['404_override'] = 'pages/error_404';

$route['(^[a-z]+([a-z]|[0-9]|-)*$)'] = "pages/select/$1";
$route['contacto'] = "pages/contact";
$route['gracias'] = "pages/thanks";
$route['gracias-por-su-compra'] = "pages/thanks_payment";
$route['nosotros'] = "pages/aboutus";
$route['anillos-de-compromiso'] = "pages/rings";
$route['anillos-de-compromiso/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/rings/$1";
$route['anillos-de-compromiso/([a-z]+([a-z]|[0-9]|-)*)/(:num)'] = "pages/rings/$1/$3";
$route['anillos-de-compromiso/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/ring/$1/$3";
$route['agregar-anillo'] = "functions/add_to_cart";
$route['carrito-de-compra'] = "pages/cart";
$route['remover-del-carrito/(:any)'] = "functions/delete_from_cart/$1";
$route['resumen-de-la-compra'] = "pages/summary_cart";
$route['conoce-tu-medida'] = "pages/know_your_measure";
$route['calidad-y-confianza'] = "pages/quality";
$route['mantenimiento-de-por-vida'] = "pages/maintenance";
$route['cancelar-compra'] = "functions/cancel_payment";
$route['busqueda'] = "pages/search";
$route['promociones'] = "pages/promotion";
$route['colecciones'] = "pages/collections";
$route['colecciones/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/collections/$1";
$route['colecciones/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/collections/$1/$3";
$route['colecciones/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/(:num)'] = "pages/collections/$1/$3/$5";
$route['colecciones/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/collection/$1/$3/$5";

$route['administrador'] = "admin/index";
/* End of file routes.php */
/* Location: ./application/config/routes.php */