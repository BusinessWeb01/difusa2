<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages";
$route['404_override'] = 'pages/error_404';
$route['actualizar'] ="panel_controller/update";
$route['ingredientes'] = "pages/ingredients";
$route['close'] = "login_controller/desloguea";
$route['login'] = "login_controller";
$route['logout'] = "logout_controller";
$route['register'] = "registra_controller";
$route['registra'] = "registra_controller/registrar";
$route['user-info'] = "panel_controller/hola";
$route['gral-info'] = "panel_controller/info";
$route['validate'] = "login_controller/loguea";
$route['comments'] = "panel_controller/comments";
$route['orders'] = "panel_controller/orders";
$route['suscription'] = "panel_controller/news";
$route['wishes'] = "panel_controller/wishes";
$route['refund'] = "panel_controller/refund";
$route['promociones'] = 'promociones/productos';
$route['order-details/(:num)'] = "panel_controller/details/$1";
$route['wishes-ins-list'] = "articulos_insumos_controller/insertWish";
$route['agrega-complemento'] = "articulos_complementos_controller/agregarProducto";
$route['agrega-insumo'] = "articulos_insumos_controller/agregarProducto";
$route['wishes-com-list'] = "articulos_complementos_controller/insertWish";
$route['Insumos-user/(:any)/(:num)'] = "articulos_insumos_controller/productos2/$1/$2";
$route['Insumos/(:any)/(:num)'] = "insumos_controller/productos2/$1/$2";
$route['search/(:num)'] = "busqueda_controller/index/$1";
$route['Complementos-user/(:any)/(:num)'] = "articulos_complementos_controller/productos2/$1/$2";
$route['Complementos/(:any)/(:num)'] = "complementos_controller/productos2/$1/$2";
$route['info-insumo/(:num)'] = "informacion_insumos/producto/$1";
$route['info-complemento/(:num)'] = "informacion_complementos/producto/$1";
$route['top_sales'] = "top_sales_controller/getTopSales";
$route['new-news'] = "panel_controller/updateNews";
$route['contacting'] = "contacto_controller/send/Contacto";
$route['team-contacting'] = "contacto_controller/send/Equipo";
$route['clientes'] = "admin_controller/allClients";
$route['ordenes'] = "admin_controller/orders";
$route['productos'] = "admin_controller/products";
$route['carrito'] = "cart_controller/inicio";
$route['activar/(:any)'] = 'activate_controller/activar/$1';
$route['info-producto/(:num)'] = "informacion/producto/$1";//ruta para insumos y complementos
$route['nuevas-adquisiciones'] = 'nuevos_controller/showNewProducts';
$route['filtrado-complementos'] = "articulos_complementos_controller/filtro/";
$route['Complementos-next/(:num)/(:num)/(:num)/(:any)/(:num)/(:num)/(:num)/(:num)'] = "complementos_controller/filtro/$1/$2/$3/$4/$5/$6/$7/$8";
$route['Insumos-next/(:num)/(:num)/(:num)/(:any)/(:num)/(:num)/(:num)/(:num)'] = "insumos_controller/filtro/$1/$2/$3/$4/$5/$6/$7/$8";
$route['Complementos-user-next/(:num)/(:num)/(:num)/(:any)/(:num)/(:num)/(:num)/(:num)'] = "articulos_complementos_controller/filtro/$1/$2/$3/$4/$5/$6/$7/$8";
$route['Insumos-user-next/(:num)/(:num)/(:num)/(:any)/(:num)/(:num)/(:num)/(:num)'] = "articulos_insumos_controller/filtro/$1/$2/$3/$4/$5/$6/$7/$8";
$route['comentar'] = "informacion_complementos/comenta";
$route['documento/(:any)'] = "pages/terms/$1";
$route['remember-passwords'] = "pages/load_remember_password";
$route['remember-password'] = "pages/remember_password";
$route['recuperar'] = "pages/reset_password";
$route['filtro/(:any)'] = "filtro_controller/filtrado/$1";
$route['manda-carro'] = "cart_controller/enviaPayPal";
$route['banea/(:num)'] = "admin_controller/banear/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */